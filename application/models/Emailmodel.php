<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailmodel extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}


	// ----------------- Registration Module ------------------- //


	function sendVerificationEmail($email, $hash)
	{

		$this->load->library('email');

		$config = array();
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "mx1.hostinger.in";
		$config['smtp_port'] = 587;
		$config['smtp_user'] = "donotreply@biddersarena.com";
		$config['smtp_pass'] = "abc123";
		$config['mailtype'] = "html";
		$config['charset'] = "iso-8859-1";
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);


		$this->email->set_newline("\n");
		$this->email->from('donotreply@biddersarena.com', "Bidders Arena");
		$this->email->to($email);
		$this->email->subject("Email Verification");
		$this->email->message("
        <p>Thanks for signing up!</p>

		<p>Your account has been created.

		Here is the email you used to signup.</p>

	  	<p>Email   :  $email</p>

      	<p>Please click the link below to activate your account:</p>

        " . base_url() . "verify/$hash
                                ");
		$this->email->send();

	}

	// ----------------- End - Registration Module ------------------- //


	// ----------------- Password Reset Module ------------------- //


	function send_reset_email($email, $reset_hash)
	{

		$this->load->library('email');

		$config = array();
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "mx1.hostinger.in";
		$config['smtp_port'] = 587;
		$config['smtp_user'] = "donotreply@biddersarena.com";
		$config['smtp_pass'] = "abc123";
		$config['mailtype'] = "html";
		$config['charset'] = "iso-8859-1";
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);


		$this->email->set_newline("\n");
		$this->email->from('donotreply@biddersarena.com', "Bidders Arena");
		$this->email->to($email);
		$this->email->subject("Reset Password");
		$this->email->message("
        <p>Forgot your password?<br><br>
		Don't worry, we got you! Let’s get you a new password.</p>

	  	<p>Email   :  $email</p>

      	<p>Please click the link below to reset your account password:</p>

        " . base_url() . "user/resetpassword/resetlink/$reset_hash
                                ");
		return $this->email->send();
	}

	// ----------------- End - Password Reset Module ------------------- //


	// ----------------- Contact Us Module ------------------- //


	function send_feedback_email($name, $email, $message)
	{

		$this->load->library('email');

		$this->email->clear();

		$config = array();
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "mx1.hostinger.in";
		$config['smtp_port'] = 587;
		$config['smtp_user'] = "support@biddersarena.com";
		$config['smtp_pass'] = "abc123";
		$config['mailtype'] = "html";
		$config['charset'] = "utf-8";
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);

		$this->email->set_newline("\n");
		$this->email->from('support@biddersarena.com');
		$this->email->to('support@biddersarena.com');
		$this->email->subject("Support Request");

		$this->email->message("
        <p>Name : $name<br>
        <p>From : $email<br><br>
		   Message: 
		   
		   $message<br>
         ");


		return $this->email->send();

		// ----------------- End - Contact Us Module ------------------- //


	}
}

