<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('emailmodel');
		$this->load->model('user_model');
	}

	// ----------------- Registration Module ------------------- //

    public function create_user($username, $email, $password, $userType)
    {

        $previous_user_id = $this->get_previous_user_id();
        if($previous_user_id == NULL) {
			$new_user_id = 1;
		} else {
			$new_user_id = ($previous_user_id + 1);
		}

		$user_data = array(
		    'user_id'    => $new_user_id,
			'username'   => $username,
			'email'      => $email,
			'user_type'  => $userType,
			'active'    => 1,
			'created_on' => date('Y-m-j H:i:s')
		);

		$password_data = array(
		    'user_id'   => $new_user_id,
            'password_hash'   => $this->hash_password($password),
            'username'   => $username,
            'email'      => $email,
			'email_verified' => 0,
			'email_hash' => hash('sha1', $email),
			'reset_hash' => NULL
        );

		$user_subscription_data = array(
			'user_id'   => $new_user_id,
			'plan_type'         =>  'free',
            'valid_till'        =>  '3000-12-31',
            'bids_remaining'    =>  6
        );

		return ($this->db->insert('user', $user_data) && $this->db->insert('password',$password_data) && $this->db->insert('user_subscription',$user_subscription_data));

	}

	// Part - Registration Module //

	private function get_previous_user_id() {

		$this->db->select('*');
		$this->db->from('user');
		$this->db->order_by("user_id","desc");
		return $this->db->get()->row('user_id');

	}

	// Part - Registration Module //

	public function get_email_hash($email) {
		$this->db->select('*');
		$this->db->from('password');
		$this->db->where('email', $email);
		return $this->db->get()->row('email_hash');
	}

	// Part - Registration Module //

	private function hash_password($password) {
		return hash('sha512', $password);
	}


	// ----------------- End - Registration Module ------------------- //



	// ----------------- Login Module ------------------- //

	public function resolve_user_login($email, $password) {

		$this->db->where('password_hash', $password);
		$this->db->where('email', $email);

		$query = $this->db->get('password');

		if($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}

	}

	// Part - Login Module //

	public function check_email_verified($email) {

		$sql = "SELECT * FROM password WHERE email = ?";
		$query = $this->db->query($sql, array($email));
		$result = $query->result();
		$ada = $result[0]->email_verified;
		if($ada == 0) {
			return false;
		} elseif($ada == 1) {
			return true;
		} else {
			return false;
		}

	}

	// Part - Login Module //

	public function get_user_id_from_email($email) {

		$this->db->select('user_id');
		$this->db->from('password');
		$this->db->where('email', $email);
		return $this->db->get()->row('user_id');

	}

	// Part - Login Module //

	public function get_user($user_id) {

		$sql = "SELECT * FROM user WHERE user_id = ?";
		$query = $this->db->query($sql, array($user_id));
		$result = $query->result();
		return $result[0];

	}

	// ----------------- End - Login Module ------------------- //


	// ----------------- Password Reset Module ------------------- //

	public function password_reset($email) {
		$result = (int)$this->verify_email($email);
		if($result == 1) {
			$reset_hash = $this->generate_reset_hash($email);
			$success = $this->emailmodel->send_reset_email($email, $reset_hash);
			if($success) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
    }

    // Part - Password Reset Module //

    public function verify_email($email) {
		$sql = "SELECT * FROM password WHERE email = ?";
		$query = $this->db->query($sql, array($email));
		$result = $query->num_rows();
		return $result;
	}

	// Part - Password Reset Module //

	public function generate_reset_hash($email) {
		$reset_hash = hash('sha512', $email.''.rand(1, 1000000));
        date_default_timezone_set('UTC');
        $current_time = date('Y-m-j H:i:s');
		$sql = "UPDATE password SET reset_hash = ? , reset_hash_time = ? WHERE email = ?";
		$this->db->query($sql, array($reset_hash, $current_time, $email));
		return $reset_hash;
	}

	// Part - Password Reset Module //

	public function verify_reset_hash($password_reset_hash) {
		$sql = "SELECT * FROM password WHERE reset_hash = ?";
		$query = $this->db->query($sql, array($password_reset_hash));
		if($query->num_rows() == 1) {
			$result = $query->result();
			return $result[0]->email;
		} else {
			return '';
		}
	}

	// Part - Password Reset Module //

	public function update_password($hash_password, $result) {
		$sql = "UPDATE password SET password_hash = ? WHERE email = ?";
		$query = $this->db->query($sql, array($hash_password, $result));
		if($query) {
			return true;
		} else {
			return false;
		}
	}

	// Part - Password Reset Module //

    public function invalidate_reset_link(){
	    $query = $this->db->query('update password set reset_hash = null, reset_hash_time = null where reset_hash_time < DATE_SUB(NOW(), INTERVAl 10 minute)');
        if($query) {
            return true;
        } else {
            return false;
        }
    }

    // ----------------- End - Password Reset Module ------------------- //


	// ----------------- Contact Us Module ------------------- //

    public function add_feedback($name,$email,$message){

		$success = $this->emailmodel->send_feedback_email($name,$email, $message);

		if($success) {
            echo "Success";
        } else {
            echo "Failed";
        }
    }

    // -------------- End - Contact Us Module ------------------ //


	// -------------- Registration Ajax Module ------------------ //

	function is_email_available($email)
	{
		$sql = "SELECT email FROM user WHERE email = ?";
		$query = $this->db->query($sql, array($email));

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function is_username_available($username)
	{
		$sql = "SELECT username FROM user WHERE username = ?";
		$query = $this->db->query($sql, array($username));

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// -------------- End - Registration Ajax Module ------------------ //


}
