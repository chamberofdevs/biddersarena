<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profilemodel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

	// -------------- User profile Module ------------------ //

	function getUserData($username) {
		$sql = "SELECT * FROM user WHERE username = ?";
		$query = $this->db->query($sql, array($username));
		$result = $query->result();

		return $result;
	}

	function findUserInfo($username) {
		$sql = "SELECT account_name, username, gender, photo, portfolio_pic1,portfolio_link1, portfolio_description1,portfolio_pic2,portfolio_link2, portfolio_description2,portfolio_pic3,portfolio_link3, portfolio_description3,portfolio_pic4,portfolio_link4, portfolio_description4, city, country, skills, professional_title, qualification, bio,certification_1,certification_1_pdf,certification_2,certification_2_pdf,certification_3,certification_3_pdf FROM user WHERE username = ?";
		$query = $this->db->query($sql, array($username));
		$result = $query->result();
		if($query->num_rows() > 0) {
			return $result;
		} else {
			return 0;
		}
	}

	function findRatingAvg($username) {
		$sql = "SELECT AVG(rating), count(review) FROM reviews WHERE review_for = ?";
		$query = $this->db->query($sql, array($username));
		$result = $query->result();

		return $result[0];
	}

	function loadReviews($username, $offset) {
		$sql = "SELECT review_by, rating, review FROM reviews WHERE review_for = ? LIMIT 4 OFFSET ?";
		$query = $this->db->query($sql, array($username, $offset));
		$result = $query->result();

		return $result;
	}

	function updateBio($username, $bio) {
		$sql = "UPDATE user SET bio = ? WHERE username = ?";
		$query = $this->db->query($sql, array($bio, $username));
		return $query;
	}

	function updateOverview($username, $name, $address1, $address2, $phone, $gender) {
		$sql = "UPDATE user SET account_name = ?, address_line1 = ?, address_line2 = ?, phone = ?, gender = ? WHERE username = ?";
		$query = $this->db->query($sql, array($name, $address1, $address2, $phone, $gender, $username));
		return $query;
	}

	function updateDescription($username, $city, $state, $country, $pincode, $skypeId, $professional_title, $qualification, $skills/*,$certification_1,$certification_1_pdf,$certification_2,$certification_2_pdf,$certification_3,$certification_3_pdf*/) {
		$sql = "UPDATE user SET city = ?, state = ?, country = ?, pincode = ?, skype_id = ?, professional_title = ?, qualification = ?, skills = ? WHERE username = ?";
		$query = $this->db->query($sql, array($city, $state, $country, $pincode, $skypeId, $professional_title, $qualification, $skills/*,$certification_1,$certification_1_pdf,$certification_2,$certification_2_pdf,$certification_3,$certification_3_pdf,*/ ,$username));
		return $query;
	}

	function updateKYC($username, $bank_account_name, $account_number, $bank_name, $bank_branch, $ifsc, $bank_address, $payment_method) {
		$sql = "UPDATE user SET bank_account_name = ?, account_no = ?, bank_name = ?, branch = ?, ifsc_code = ?, branch_address = ?, payment_method = ? WHERE username = ?";
		$query = $this->db->query($sql, array($bank_account_name, $account_number, $bank_name, $bank_branch, $ifsc, $bank_address, $payment_method, $username));
		return $query;
	}

	// -------------- End - User profile Module ------------------ //


    public function updateCertificate1($certification_1,$certification_1_pdf,$username){
        $sql = "update user set certification_1 = ?,certification_1_pdf = ? where username = ?";
        $query = $this->db->query($sql,array($certification_1,$certification_1_pdf,$username));
        return $query;
    }

    public function updateCertificate2($certification_2,$certification_2_pdf,$username){
        $sql = "update user set certification_2 = ?,certification_2_pdf = ? where username = ?";
        $query = $this->db->query($sql,array($certification_2,$certification_2_pdf,$username));
        return $query;
    }

    public function updateCertificate3($certification_3,$certification_3_pdf,$username){
        $sql = "update user set certification_3 = ?,certification_3_pdf = ? where username = ?";
        $query = $this->db->query($sql,array($certification_3,$certification_3_pdf,$username));
        return $query;
    }

    public function updatePortfolio1($pic_path1, $portfolioLink1, $portfolioDesc1,$username){
        $sql = "update user set portfolio_pic1 = ?,portfolio_link1 = ?, portfolio_description1 = ? where username = ?";
        $query = $this->db->query($sql,array($pic_path1, $portfolioLink1, $portfolioDesc1,$username));
        return $query;
    }

    public function updatePortfolio2($pic_path2, $portfolioLink2, $portfolioDesc2,$username){
        $sql = "update user set portfolio_pic2 = ?,portfolio_link2 = ?, portfolio_description2 = ? where username = ?";
        $query = $this->db->query($sql,array($pic_path2, $portfolioLink2, $portfolioDesc2,$username));
        return $query;
    }

    public function updatePortfolio3($pic_path3, $portfolioLink3, $portfolioDesc3,$username){
        $sql = "update user set portfolio_pic3 = ?,portfolio_link3 = ?, portfolio_description3 = ? where username = ?";
        $query = $this->db->query($sql,array($pic_path3, $portfolioLink3, $portfolioDesc3,$username));
        return $query;
    }

    public function updatePortfolio4($pic_path4, $portfolioLink4, $portfolioDesc4,$username){
        $sql = "update user set portfolio_pic4 = ?,portfolio_link4 = ?, portfolio_description4 = ? where username = ?";
        $query = $this->db->query($sql,array($pic_path4, $portfolioLink4, $portfolioDesc4,$username));
        return $query;
    }
}
