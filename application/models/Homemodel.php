<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homemodel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->library('email');
	}

	// -------------- User Verification Module ------------------ //

	function verifyEmailAddress($verification_code){
		$value =  $this->no_rows($verification_code);
		if($value == 0) {
			return 0;
		} elseif ($value == 1) {
			$sql = "UPDATE password SET email_verified = 1 WHERE email_hash = ?";
			$this->db->query($sql, array($verification_code));
			return 1;
		}
	}

	function no_rows($verification_code){
		$sql = "SELECT count(*) FROM password WHERE email_hash = ?";
		$query = $this->db->query($sql, array($verification_code));
		return $query->num_rows();
	}

	// -------------- End - User Verification Module ------------------ //

}
