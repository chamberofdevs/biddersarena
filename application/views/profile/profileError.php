<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<meta name="description" content="">
	<title>Bidder's Arena | Profile</title>

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">

	<!-- Countries selector js file -->
	<script src="<?php echo base_url();?>assets/js/countries.js"></script>

	<!--  Modal  -->
	<style>

		.modal {
			bottom: 0;
			left: 0;
			position: absolute;
			right: 0;
			top: 0;
			-webkit-box-align: center;
			-ms-flex-align: center;
			align-items: center;
			display: none;
			-webkit-box-pack: center;
			-ms-flex-pack: center;
			justify-content: center;
			overflow: hidden;
			position: fixed;
			z-index: 40;
		}

		.modal.is-active {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}

		.modal-background {
			bottom: 0;
			left: 0;
			position: absolute;
			right: 0;
			top: 0;
			background-color: rgba(10, 10, 10, 0.86);
		}

		.modal-content,
		.modal-card {
			margin: 0 20px;
			max-height: calc(100vh - 160px);
			overflow: auto;
			position: relative;
			width: 100%;
		}

		@media screen and (min-width: 769px), print {
			.modal-content,
			.modal-card {
				margin: 0 auto;
				max-height: calc(100vh - 40px);
				width: 640px;
			}
		}

		.modal-close {
			-webkit-touch-callout: none;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			-moz-appearance: none;
			-webkit-appearance: none;
			background-color: rgba(10, 10, 10, 0.2);
			border: none;
			border-radius: 290486px;
			cursor: pointer;
			display: inline-block;
			-webkit-box-flex: 0;
			-ms-flex-positive: 0;
			flex-grow: 0;
			-ms-flex-negative: 0;
			flex-shrink: 0;
			font-size: 0;
			height: 20px;
			max-height: 20px;
			max-width: 20px;
			min-height: 20px;
			min-width: 20px;
			outline: none;
			position: relative;
			vertical-align: top;
			width: 20px;
			background: none;
			height: 40px;
			position: fixed;
			right: 20px;
			top: 20px;
			width: 40px;
		}

		.modal-close:before, .modal-close:after {
			background-color: white;
			content: "";
			display: block;
			left: 50%;
			position: absolute;
			top: 50%;
			-webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);
			transform: translateX(-50%) translateY(-50%) rotate(45deg);
			-webkit-transform-origin: center center;
			transform-origin: center center;
		}

		.modal-close:before {
			height: 2px;
			width: 50%;
		}

		.modal-close:after {
			height: 50%;
			width: 2px;
		}

		.modal-close:hover, .modal-close:focus {
			background-color: rgba(10, 10, 10, 0.3);
		}

		.modal-close:active {
			background-color: rgba(10, 10, 10, 0.4);
		}

		.modal-close.is-small {
			height: 16px;
			max-height: 16px;
			max-width: 16px;
			min-height: 16px;
			min-width: 16px;
			width: 16px;
		}

		.modal-close.is-medium {
			height: 24px;
			max-height: 24px;
			max-width: 24px;
			min-height: 24px;
			min-width: 24px;
			width: 24px;
		}

		.modal-close.is-large {
			height: 32px;
			max-height: 32px;
			max-width: 32px;
			min-height: 32px;
			min-width: 32px;
			width: 32px;
		}

		.modal-card {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-orient: vertical;
			-webkit-box-direction: normal;
			-ms-flex-direction: column;
			flex-direction: column;
			max-height: calc(100vh - 40px);
			overflow: hidden;
		}

		.modal-card-head,
		.modal-card-foot {
			-webkit-box-align: center;
			-ms-flex-align: center;
			align-items: center;
			background-color: whitesmoke;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-ms-flex-negative: 0;
			flex-shrink: 0;
			-webkit-box-pack: start;
			-ms-flex-pack: start;
			justify-content: flex-start;
			padding: 20px;
			position: relative;
		}

		.modal-card-head {
			border-bottom: 1px solid #dbdbdb;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
		}

		.modal-card-title {
			color: #363636;
			-webkit-box-flex: 1;
			-ms-flex-positive: 1;
			flex-grow: 1;
			-ms-flex-negative: 0;
			flex-shrink: 0;
			font-size: 1.5rem;
			line-height: 1;
		}

		.modal-card-foot {
			border-bottom-left-radius: 5px;
			border-bottom-right-radius: 5px;
			border-top: 1px solid #dbdbdb;
		}

		.modal-card-foot .button:not(:last-child) {
			margin-right: 10px;
		}

		.modal-card-body {
			-webkit-overflow-scrolling: touch;
			background-color: white;
			-webkit-box-flex: 1;
			-ms-flex-positive: 1;
			flex-grow: 1;
			-ms-flex-negative: 1;
			flex-shrink: 1;
			overflow: auto;
			padding: 20px;
		}
	</style>

	<style>
		.modal {
			overflow: hidden;
		}
		.modal .uploadImg .modalTitle {
			text-align: center;
			font-size: 2rem;
			font-family: "Open Sans", sans-serif;
			font-weight: bold;
			padding: 1rem 0px 0rem;
			margin: 0;
		}
	</style>

	<!--  Custom  -->
	<style>
		body {
			user-select: none;
			background: #efefef;
		}
		.alertMsg {
			position: absolute;
			top: 0px;
			z-index: 1035;
			height: 60.8px;
			width: 100vw;
			text-align: center;
			background: #66BB6A;
			color: #FFF;
			font-size: 18px;
			line-height: 60.8px;

			animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
		}
		@keyframes fadeOutTwoSec {
			0%{
				top: 0;
			}
			100%{
				top: -65px;
			}
		}

		.mbr-iconfont {
			font-size: 1.3rem !important;
		}

		.profileIconNav {
			height: 35px;
			width: 35px;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			margin-right: 10px;
			overflow: hidden;
			background-size: cover;
			background-position: center top;
		}

		.dropdown-submenu {
			left: 0 !important;
			transform: translateX(-100%) !important;
		}
		.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
			left: 1.1538em !important;
			transform: rotate(180deg) !important;
		}
		@media (max-width: 991px) {
			.dropdown-submenu {
				left: 100% !important;
			}
			.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
				right: 1.1538em !important;
				transform: rotate(0deg) !important;
				border-bottom: 0em solid transparent !important;
			}
		}

		.cards {
			display: none;
		}
		.is-shown {
			display: block;
		}

		.wrapper {
			padding-top: 100px;
			padding-bottom: 40px;
			color: #999999;
			box-sizing: border-box;
		}
		.is-card {
			min-height: 200px;
			background: white;
			padding: 20px;
		}

		.is-card-left .imgWrapper {
			width: 80%;
			margin-left: 10%;
			margin-bottom: 20px;
			overflow: hidden;
			border-radius: 3px;
		}
		.is-card-left .imgWrapper .image {
			position:relative;
			overflow:hidden;
			padding-bottom:100%;
		}
		.is-card-left .imgWrapper .img {
			position: absolute;
			height: 100%;
			width: 100%;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			background-size: cover;
			background-position: center top;
		}
		.is-card-left .imgWrapper .image .changeDP {
			color: black;
			position: absolute;
			right: 0;
			top: 0;
			font-size: 1.1rem;
			padding: 3.5px 8px;
			background: rgba(255, 255, 255, 0.4);
			text-align: right;
			border-bottom-left-radius: 3px;
		}
		.is-card-left .imgWrapper .image .changeDP:hover {
			background: rgba(255, 255, 255, 0.8);
			cursor: pointer;
		}

		.is-card-left .sectionController .btn {
			display: block;
			padding: 10px 10px;
			width: 80%;
			margin: 0 auto;
			border-radius: 0 !important;
			border: none !important;
			box-shadow: none !important;
		}
		.is-card-left .sectionController .btn:nth-child(1) {
			border-radius: 3px 3px 0px 0px !important;
		}
		.is-card-left .sectionController .btn:last-child {
			border-radius: 0px 0px 3px 3px !important;
		}

		.is-card-left hr {
			border-top-width: 1.5px;
			margin-top: 1.4rem;
		}

		.is-card-left .buttonSection .btn {
			padding: 10px 10px;
			width: 180px;
			max-width: 80%;
		}
		.btn-warning {
			background-color: rgb(247, 237, 74);
			color: rgb(63, 60, 3);
			border-color: rgb(247, 237, 74);
		}
		.btn-warning:hover,
		.btn-warning:focus {
			background-color: rgb(234, 221, 10);
			border-color: rgb(234, 221, 10);
			color: rgb(63, 60, 3);
		}
		.btn-danger {
			background-color: #ff3366;
			border-color: #ff3366;
			color: #ffffff;
		}
		.btn-danger:hover,
		.btn-danger:focus {
			background-color: #e50039;
			border-color: #e50039;
		}
		.btn-info {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
		}
		.btn-info:hover,
		.btn-info:focus {
			background-color: #0d6786;
			border-color: #0d6786;
		}
		.btn a {
			text-decoration: none;
			color: white;
		}
		.btn-warning a {
			color: rgb(63, 60, 3);
		}
		.viewAs {
			padding: 0.4rem 1rem;
			width: 80%;
			margin: 1rem 10% 0;
			color: #149dcc;
			background-color: transparent;
			border-color: #149dcc;
		}
		.viewAs:hover,
		.viewAs:focus {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
		}

		.is-card-left .sectionController .is-pressed {
			background-color: #0d6786;
			border-color: #0d6786;
		}
	</style>

	<!--  Custom  -->
	<style>
		.is-card-right .title p {
			font-size: 2rem;
			font-weight: 800;
			font-family: "Josefin Slab", monospace;
			color: #0d6786;
			padding: 0;
			margin: 0;
		}
		.is-card-right .title .titleUnderline {
			display: block;
			height: 2px;
			width: 60px;
			background: #0d6786;
			border-radius: 5px;
			margin-bottom: 1.4rem;
		}
		.is-card-right .title .edit {
			cursor: pointer;
			font-size: 14px;
			position: absolute;
			top: 36.5px;
			right: 40px;
			color: #BDBDBD;
			font-weight: 500;
			font-family: "Open Sans", sans-serif;
			letter-spacing: 0.5px;
		}
		.is-card-right .title .edit .fa {
			padding-right: 5px;
		}
		.is-card-right .title .edit:hover,
		.is-card-right .title .edit:focus {
			color: #4c4c4c;
		}

		.lineBreak {
			padding: 0;
			border: none;
			margin: 0.85rem 0px;
		}
		.is-card-right .textContent {
			width: 100%;
			word-wrap: break-word;
		}
		.is-card-right .textContent .textTitle {
			font-family: "Open Sans", sans-serif;
			padding-right: 5px;
			color: #757575;
			font-weight: 600;
			font-size: 1.2rem;
			width: 40%;
			max-width: 180px;
			display: inline-block;
		}
		.is-card-right .textContent .textData {
			font-size: 1.2rem;
			width: 56%;
			display: inline-block;
		}

		.titleInfo,
		.professionalTitle,
		.tab-contents .overviewTab p {
			padding: 0;
			margin: 0;
		}
		.titleInfo .username {
			font-family: "Josefin Slab", "Arvo", "Open Sans";
			color: #212121;
			font-size: 2.5rem;
			font-weight: bold;
		}
		.titleInfo .fa {
			font-size: 1.1rem;
			padding-left: 10px;
			padding-right: 2px;
			color: #9E9E9E;
			text-transform: capitalize;
		}
		.titleInfo .location {
			font-size: 0.9rem;
			color: #9E9E9E;
			font-family: "Open Sans", sans-serif;
			text-transform: capitalize;
		}
		.professionalTitle {
			font-size: 1rem;
			color: #2962FF;
			font-weight: 600;
			font-family: "Open Sans", sans-serif;
		}

		.tabs .tab-links .edit {
			display: none;
			cursor: pointer;
			font-size: 14px;
			position: absolute;
			top: 138px;
			right: 40px;
			color: #BDBDBD;
			font-weight: 500;
			font-family: "Open Sans", sans-serif;
			letter-spacing: 0.5px;
		}
		.tabs .tab-links .edit .fa {
			padding-right: 5px;
		}
		.tabs .tab-links .editActive {
			display: inline-block;
		}
		.tabs .tab-links .edit:hover,
		.tabs .tab-links .edit:focus {
			color: #4c4c4c;
		}

		.tabs .tab-links .save {
			padding: 0.3rem 0.5rem;
			background-color: #149dcc;
			color: #fff !important;
			border-color: #149dcc;
			margin: 0;
			right: 35px !important;
		}
		.tabs .tab-links .save:hover {
			background-color: #0d6786;
			border-color: #0d6786;
		}
		.tabs .tab-links .save:focus,
		.tabs .tab-links .save:active {
			background-color: #0d6786;
			border-color: #0d6786;
			box-shadow: 0 0 0 3px #149dcc;
		}
		.tabs .tab-links .cancel {
			padding: 0.3rem 0.5rem;
			background-color: #ff3366;
			border-color: #ff3366;
			color: #fff !important;
			margin: 0;
			right: 110px !important;
		}
		@media (max-width: 767px) {
			.tabs .tab-links .save {
				top: 105px !important;
			}
			.tabs .tab-links .cancel {
				right: 35px !important;
				top: 138px !important;
			}
		}
		.tabs .tab-links .cancel:hover {
			background-color: #e50039;
			border-color: #e50039;
		}
		.tabs .tab-links .cancel:focus,
		.tabs .tab-links .cancel:active {
			background-color: #e50039;
			border-color: #e50039;
			box-shadow: 0 0 0 3px rgba(220,53,69,.5);
		}

		.tab-contents .overviewTab {
			font-family: "Open Sans", sans-serif;
		}
		.tab-contents .overviewTab .muted {
			text-transform: uppercase;
			font-size: 0.6rem;
			letter-spacing: 0.15rem;
			color: #BDBDBD;
			font-weight: bold;
			padding: 18px 0;
		}
		.tab-contents .overviewTab .dataLine {
			padding: 6px 0px;
		}
		.tab-contents .overviewTab .dataLine .dataTitle {
			font-weight: 600;
			display: inline-block;
			width: 100px;
			color: #757575;
		}
		.tab-contents .overviewTab .dataLine .dataContent {
			font-size: 1.1rem;
		}

		.tab-contents .bioTab .bioText {
			font-size: 0.95rem;
			width: 90%;
			color: #757575;
			padding-top: 15px;
		}
		#bioTextareaHelp > p {
			margin: 0;
		}

		/*  Review Tab  */
		.reviewCol {
			margin-bottom: 25px;
		}
		.reviewCol .reviewRating {height: 40px;}

		.reviewCol .reviewRatingDivOne {height: 100%; width: 40px; float: left; border-radius: 2px; background: #2185d5; text-align: center;}
		.reviewCol .reviewRatingDivTwo {height: 100%; width: auto; float: left; padding-left: 15px;}
		.reviewCol .reviewRatingDivTwoOne {height:  25px; width:  auto; line-height: 25px;}
		.reviewCol .reviewRatingDivTwoTwo {height: 15px; line-height: 15px;}

		.reviewCol .ratingNumber {
			line-height: 40px;
			color: white;
			font-weight: bold;
			font-size: 1.1rem;
		}
		.reviewCol .reviewData {
			margin: 15px 0px 0px;
			color: #757575;
			font-size: 1rem;
			line-height: 1.4rem;
		}

		.reviewCol .rating > input {
			display: none;
		}
		.reviewCol .rating > label:before {
			font-size: 1.2rem;
			font-family: FontAwesome;
			display: block;
			content: "\f005";
		}
		.reviewCol .rating > label {
			color: #2185d5;
		}
		.reviewCol .rating > input:checked ~ label {
			color: #ddd;
		}
		.reviewCol .rating > input:checked + label {
			color: #2185d5;
		}
	</style>

	<!--  Tabs  -->
	<style>
		/*----- Tabs -----*/
		.tabs {
			width:100%;
			display:inline-block;
		}
		/*----- Tab Links -----*/
		/* Clearfix */
		.tab-links:after {
			display:block;
			clear:both;
			content:'';
		}
		.tab-links {
			padding-left: 0;
			margin-bottom: 15px;
		}
		.tab-links li {
			margin: 0px 25px 0px 0px;
			float:left;
			list-style:none;
			background:#fff;
			border-bottom: 1.5px solid transparent;
		}
		.tab-links i {
			padding-right: 10px;
			font-size: 0.8em;
		}
		.tab-links a {
			padding:8px 2px 8px;
			display:inline-block;
			font-size:16px;
			font-weight:600;
			color:#4c4c4c;
			transition:all linear 0.15s;
		}
		.tab-links a:hover {
			text-decoration:none;
		}
		li.active a, li.active a:hover {
			color:#4c4c4c;
			border-bottom: 1.5px solid #2185d5;
		}
		.tab {
			display:none;
		}
		.tab.active {
			display:block;
		}
	</style>

	<style>
		.cards form .form-control {
			min-height: 0;
			width: 100%;
			padding: .5rem .75rem;
			font-size: 1rem;
			line-height: 1.25;
		}
		.cards form .labelInput {
			font-weight: 600;
			color: #757575;
		}
		.textareaFixedSize {
			resize: none;
			overflow: hidden;
		}
		.cards form .fileColumn {
			padding-bottom: 6px;
		}
		.cards form .form-control-file {
			width: 100%;
		}
		.cards .save {
			padding: 0.3rem 0.5rem;
			background-color: #149dcc;
			color: #fff !important;
			border-color: #149dcc;
			margin: 0;
			right: 35px !important;
		}
		.cards .save:hover {
			background-color: #0d6786;
			border-color: #0d6786;
		}
		.cards .save:focus,
		.cards .save:active {
			background-color: #0d6786;
			border-color: #0d6786;
			box-shadow: 0 0 0 3px #149dcc;
		}
		.cards .cancel {
			padding: 0.3rem 0.5rem;
			background-color: #ff3366;
			border-color: #ff3366;
			color: #fff !important;
			margin: 0;
			right: 110px !important;
		}
		@media (max-width: 767px) {
			.cards .cancel {
				right: 35px !important;
				top: 70px !important;
			}
		}
		.cards .cancel:hover {
			background-color: #e50039;
			border-color: #e50039;
		}
		.cards .cancel:focus,
		.cards .cancel:active {
			background-color: #e50039;
			border-color: #e50039;
			box-shadow: 0 0 0 3px rgba(220,53,69,.5);
		}



		/*  PDF Icon  */
		.pdfFile {
			padding-right: 85px;
			width: 150px;
			cursor: pointer;
			margin-top: 1rem;
			text-align: center;
		}
		.pdfFile:hover > .pdf-icon:before {
			color: #4c4c4c;
		}
		.pdfFile .pdf-icon {
			font-size: 100%;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			display: inline-block;
			position: relative;
			width: 3em;
			height: 4.25em;
			background-color: #eee;
			background-image: url("https://i.imgur.com/lZ5SgDE.png");
			background-repeat: no-repeat;
			-webkit-background-size: 85% auto;
			-moz-background-size: 85% auto;
			background-size: 85% auto;
			background-position: center 1rem;
			border-radius: 1px 1em 1px 1px;
			border: 1px solid #ddd;
		}
		.pdfFile .pdf-icon:after {
			content: 'PDF';
			font-family: Arial;
			font-weight: bold;
			font-size: 0.6rem;
			text-align: center;
			padding: 0.2em 0 0.1em;
			color: #fff;
			display: block;
			position: absolute;
			top: 0.7em;
			left: -1.5em;
			width: 3.4em;
			height: auto;
			background: #da2525;
			border-radius: 2px;
		}
		.pdfFile .pdf-icon:before {
			font-size: 0.8rem;
			position: absolute;
			bottom: 0px;
			left: 58px;
			width: 60px;
			word-wrap: break-word;
		}
	</style>

	<!--  For pdf file name  -->
	<style>
		/*  File 1 name  */
		#pdfFile1 .pdf-icon:before {
			content: "<?php if (isset($certification_1)) : ?><?= $certification_1 ?><?php endif; ?>";
		}

		/*  File 2 name  */
		#pdfFile2 .pdf-icon:before {
			content: "<?php if (isset($certification_2)) : ?><?= $certification_2 ?><?php endif; ?>";
		}

		/*  File 3 name  */
		#pdfFile3 .pdf-icon:before {
			content: "<?php if (isset($certification_3)) : ?><?= $certification_3 ?><?php endif; ?>";
		}


		/*  Tags  */
		.tag {
			border-radius: 3px 0 0 3px;
			display: inline-block;
			height: 24px;
			line-height: 24px;
			padding: 0 20px 0 23px;
			position: relative;
			margin: 0 10px 10px 0;
			text-decoration: none;
			-webkit-transition: color 0.2s;
			color: white;
			background-color: #2185d5;
			font-size: 1rem;
		}
		.tag::before {
			background: #fff;
			border-radius: 10px;
			box-shadow: inset 0 1px rgba(0, 0, 0, 0.25);
			content: '';
			height: 6px;
			left: 10px;
			position: absolute;
			width: 6px;
			top: 10px;
		}
		.tag::after {
			background: #fff;
			border-bottom: 12px solid transparent;
			border-left: 10px solid #eee;
			border-top: 12px solid transparent;
			content: '';
			position: absolute;
			right: 0;
			top: 0;
			border-left-color: #2185d5;
		}
	</style>
</head>

<body>

<?php if ($this->session->flashdata('msg')) : ?>
	<div class="alertMsg">
		<?= $this->session->flashdata('msg') ?>
	</div>
<?php endif; ?>

<?php if ($this->session->flashdata('msg')) : ?>
	<div class="alertMsg">
		<?= $this->session->flashdata('msg'); ?>
	</div>
<?php endif; ?>

<section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm">

		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>

		<div class="menu-logo">
			<div class="navbar-brand">

                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-cash mbr-iconfont mbr-iconfont-btn"></span>My Wallet<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-bookmark mbr-iconfont mbr-iconfont-btn"></span>My Projects<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-sites mbr-iconfont mbr-iconfont-btn"></span>Dashboard<br></a></li>

				<li class="nav-item dropdown">
					<a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
						<span class="profileIconNav"></span><?= $this->session->userdata('username') ?><br></a>

					<div class="dropdown-menu">

						<div class="dropdown">
							<a class="dropdown-item text-white dropdown-toggle display-4" data-toggle="dropdown-submenu" aria-expanded="false">Help</a>

							<div class="dropdown-menu dropdown-submenu">
								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">FAQ</a>

								<a class="dropdown-item text-white display-4" href="#">Contact Us</a>

								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Our Charges</a>
							</div>
						</div>

						<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Post Project</a>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>

					</div>
				</li>
			</ul>
		</div>
	</nav>
</section>

<div class="container wrapper">
	<div class="row">

		<img src="<?php echo base_url();?>assets/assets/images/not-found.jpg" height="100%" width="100%">

	</div>
</div>

<script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>

</body>

</html>
