<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<meta name="description" content="">
	<title>Bidder's Arena | Profile</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">
    <!-- TagsInput CSS file -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tagsinput.css" type="text/css">

	<!-- Countries selector js file -->
	<script src="<?php echo base_url();?>assets/js/countries.js"></script>

    <!--  Modal  -->
    <style>
        .modal {
          bottom: 0;
          left: 0;
          position: absolute;
          right: 0;
          top: 0;
          -webkit-box-align: center;
              -ms-flex-align: center;
                  align-items: center;
          display: none;
          -webkit-box-pack: center;
              -ms-flex-pack: center;
                  justify-content: center;
          overflow: hidden;
          position: fixed;
          z-index: 40;
        }

        .modal.is-active {
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
        }

        .modal-background {
          bottom: 0;
          left: 0;
          position: absolute;
          right: 0;
          top: 0;
          background-color: rgba(10, 10, 10, 0.86);
        }

        .modal-content,
        .modal-card {
          margin: 0 20px;
          max-height: calc(100vh - 160px);
          overflow: auto;
          position: relative;
          width: 100%;
        }

        @media screen and (min-width: 769px), print {
          .modal-content,
          .modal-card {
            margin: 0 auto;
            max-height: calc(100vh - 40px);
            width: 640px;
          }
        }

        .modal-close {
          -webkit-touch-callout: none;
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          user-select: none;
          -moz-appearance: none;
          -webkit-appearance: none;
          background-color: rgba(10, 10, 10, 0.2);
          border: none;
          border-radius: 290486px;
          cursor: pointer;
          display: inline-block;
          -webkit-box-flex: 0;
              -ms-flex-positive: 0;
                  flex-grow: 0;
          -ms-flex-negative: 0;
              flex-shrink: 0;
          font-size: 0;
          height: 20px;
          max-height: 20px;
          max-width: 20px;
          min-height: 20px;
          min-width: 20px;
          outline: none;
          position: relative;
          vertical-align: top;
          width: 20px;
          background: none;
          height: 40px;
          position: fixed;
          right: 20px;
          top: 20px;
          width: 40px;
        }

        .modal-close:before, .modal-close:after {
          background-color: white;
          content: "";
          display: block;
          left: 50%;
          position: absolute;
          top: 50%;
          -webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);
                  transform: translateX(-50%) translateY(-50%) rotate(45deg);
          -webkit-transform-origin: center center;
                  transform-origin: center center;
        }

        .modal-close:before {
          height: 2px;
          width: 50%;
        }

        .modal-close:after {
          height: 50%;
          width: 2px;
        }

        .modal-close:hover, .modal-close:focus {
          background-color: rgba(10, 10, 10, 0.3);
        }

        .modal-close:active {
          background-color: rgba(10, 10, 10, 0.4);
        }

        .modal-close.is-small {
          height: 16px;
          max-height: 16px;
          max-width: 16px;
          min-height: 16px;
          min-width: 16px;
          width: 16px;
        }

        .modal-close.is-medium {
          height: 24px;
          max-height: 24px;
          max-width: 24px;
          min-height: 24px;
          min-width: 24px;
          width: 24px;
        }

        .modal-close.is-large {
          height: 32px;
          max-height: 32px;
          max-width: 32px;
          min-height: 32px;
          min-width: 32px;
          width: 32px;
        }

        .modal-card {
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-orient: vertical;
          -webkit-box-direction: normal;
              -ms-flex-direction: column;
                  flex-direction: column;
          max-height: calc(100vh - 40px);
          overflow: hidden;
        }

        .modal-card-head,
        .modal-card-foot {
          -webkit-box-align: center;
              -ms-flex-align: center;
                  align-items: center;
          background-color: whitesmoke;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -ms-flex-negative: 0;
              flex-shrink: 0;
          -webkit-box-pack: start;
              -ms-flex-pack: start;
                  justify-content: flex-start;
          padding: 20px;
          position: relative;
        }

        .modal-card-head {
          border-bottom: 1px solid #dbdbdb;
          border-top-left-radius: 5px;
          border-top-right-radius: 5px;
        }

        .modal-card-title {
          color: #363636;
          -webkit-box-flex: 1;
              -ms-flex-positive: 1;
                  flex-grow: 1;
          -ms-flex-negative: 0;
              flex-shrink: 0;
          font-size: 1.5rem;
          line-height: 1;
        }

        .modal-card-foot {
          border-bottom-left-radius: 5px;
          border-bottom-right-radius: 5px;
          border-top: 1px solid #dbdbdb;
        }

        .modal-card-foot .button:not(:last-child) {
          margin-right: 10px;
        }

        .modal-card-body {
          -webkit-overflow-scrolling: touch;
          background-color: white;
          -webkit-box-flex: 1;
              -ms-flex-positive: 1;
                  flex-grow: 1;
          -ms-flex-negative: 1;
              flex-shrink: 1;
          overflow: auto;
          padding: 20px;
        }
    </style>
    
    <!--  Custom Modal  -->
    <style>
        .modal {
            overflow: hidden;
        }
        .modal .uploadImg .modalTitle {
            text-align: center;
            font-size: 2rem;
            font-family: "Open Sans", sans-serif;
            font-weight: bold;
            padding: 1rem 0px 0rem;
            margin: 0;
        }

        .input-group input[type=search]:focus {
            border-color: #e8e8e8;
        }
        .input-group .input-group-addon button[type=button]:focus {
            box-shadow: none;
        }
    </style>

    <!--  Custom left section  -->
	<style>
		body {
            user-select: none;
			background: #efefef;
		}
		.alertMsg {
			position: absolute;
			top: 0px;
			z-index: 1035;
			height: 60.8px;
			width: 100vw;
			text-align: center;
			background: #66BB6A;
			color: #FFF;
			font-size: 18px;
			line-height: 60.8px;

			animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
		}
		@keyframes fadeOutTwoSec {
			0%{
				top: 0;
			}
			100%{
				top: -65px;
			}
		}

		.mbr-iconfont {
			font-size: 1.3rem !important;
		}

		.profileIconNav {
			height: 35px;
			width: 35px;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			margin-right: 10px;
			overflow: hidden;
			background-size: cover;
			background-position: center top;
		}

		.dropdown-submenu {
			left: 0 !important;
			transform: translateX(-100%) !important;
		}
		.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
			left: 1.1538em !important;
			transform: rotate(180deg) !important;
		}
		@media (max-width: 991px) {
			.dropdown-submenu {
				left: 100% !important;
			}
			.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
				right: 1.1538em !important;
				transform: rotate(0deg) !important;
				border-bottom: 0em solid transparent !important;
			}
		}

        .cards {
            display: none;
        }
		.is-shown {
			display: block;
		}

		.wrapper {
			padding-top: 100px;
			padding-bottom: 40px;
			color: #999999;
			box-sizing: border-box;
		}
		.is-card {
			min-height: 200px;
			background: white;
			padding: 20px;
		}

		.is-card-left .imgWrapper {
			width: 80%;
			margin-left: 10%;
			margin-bottom: 20px;
			overflow: hidden;
			border-radius: 3px;
		}
		.is-card-left .imgWrapper .image {
			position:relative;
			overflow:hidden;
			padding-bottom:100%;
		}
		.is-card-left .imgWrapper .img {
			position: absolute;
			height: 100%;
			width: 100%;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			background-size: cover;
			background-position: center top;
		}
		.is-card-left .imgWrapper .image .changeDP {
            color: black;
            position: absolute;
            right: 0;
            top: 0;
            font-size: 1.1rem;
            padding: 3.5px 8px;
            background: rgba(255, 255, 255, 0.4);
            text-align: right;
            border-bottom-left-radius: 3px;
        }
		.is-card-left .imgWrapper .image .changeDP:hover {
            background: rgba(255, 255, 255, 0.8);
            cursor: pointer;
        }

		.is-card-left .sectionController .btn {
			display: block;
			padding: 10px 10px;
			width: 80%;
			margin: 0 auto;
			border-radius: 0 !important;
			border: none !important;
			box-shadow: none !important;
		}
		.is-card-left .sectionController .btn:nth-child(1) {
			border-radius: 3px 3px 0px 0px !important;
		}
		.is-card-left .sectionController .btn:last-child {
			border-radius: 0px 0px 3px 3px !important;
		}

		.is-card-left hr {
			border-top-width: 1.5px;
			margin-top: 1.4rem;
		}

		.is-card-left .buttonSection .btn {
			padding: 10px 10px;
			width: 180px;
			max-width: 80%;
		}
		.btn-warning {
			background-color: rgb(247, 237, 74);
			color: rgb(63, 60, 3);
			border-color: rgb(247, 237, 74);
		}
		.btn-warning:hover,
		.btn-warning:focus {
			background-color: rgb(234, 221, 10);
			border-color: rgb(234, 221, 10);
			color: rgb(63, 60, 3);
		}
		.btn-danger {
			background-color: #ff3366;
			border-color: #ff3366;
			color: #ffffff;
		}
		.btn-danger:hover,
		.btn-danger:focus {
			background-color: #e50039;
			border-color: #e50039;
		}
		.btn-info {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
		}
		.btn-info:hover,
		.btn-info:focus {
			background-color: #0d6786;
			border-color: #0d6786;
		}
        .btn a {
            text-decoration: none;
            color: white;
        }
        .btn-warning a {
			color: rgb(63, 60, 3);
        }
        .viewAs {
            padding: 0.4rem 1rem;
            width: 80%;
            margin: 1rem 10% 0;
            color: #149dcc;
            background-color: transparent;
            border-color: #149dcc;
        }
        .viewAs:hover,
        .viewAs:focus {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
        }

		.is-card-left .sectionController .is-pressed {
			background-color: #0d6786;
			border-color: #0d6786;
		}
	</style>

	<!--  Custom rigth section  -->
	<style>
		.is-card-right .title p {
			font-size: 2rem;
			font-weight: 800;
			font-family: "Josefin Slab", monospace;
			color: #0d6786;
			padding: 0;
			margin: 0;
		}
		.is-card-right .title .titleUnderline {
			display: block;
			height: 2px;
			width: 60px;
			background: #0d6786;
			border-radius: 5px;
			margin-bottom: 1.4rem;
		}
		.is-card-right .title .edit {
            cursor: pointer;
            font-size: 14px;
            position: absolute;
            top: 36.5px;
            right: 40px;
            color: #BDBDBD;
            font-weight: 500;
            font-family: "Open Sans", sans-serif;
            letter-spacing: 0.5px;
		}
		.is-card-right .title .edit .fa {
            padding-right: 5px;
		}
		.is-card-right .title .edit:hover,
        .is-card-right .title .edit:focus {
            color: #4c4c4c;
		}

        .lineBreak {
            padding: 0;
            border: none;
            margin: 0.85rem 0px;
        }
        .is-card-right .textContent {
            width: 100%;
            word-wrap: break-word;
        }
		.is-card-right .textContent .textTitle {
			font-family: "Open Sans", sans-serif;
			padding-right: 5px;
            color: #757575;
            font-weight: 600;
            font-size: 1rem;
            width: 40%;
            max-width: 180px;
            display: inline-block;
		}
		.is-card-right .textContent .textData {
			font-size: 1rem;
            width: 56%;
            display: inline-block;
		}

		.titleInfo,
		.professionalTitle,
		.tab-contents .overviewTab p {
			padding: 0;
			margin: 0;
		}
		.titleInfo .username {
			font-family: "Josefin Slab", "Arvo", "Open Sans";
			color: #212121;
			font-size: 2.5rem;
			font-weight: bold;
		}
		.titleInfo .fa {
			font-size: 1.1rem;
			padding-left: 10px;
			padding-right: 2px;
			color: #9E9E9E;
            text-transform: capitalize;
		}
		.titleInfo .location {
			font-size: 0.9rem;
			color: #9E9E9E;
			font-family: "Open Sans", sans-serif;
            text-transform: capitalize;
		}
		.professionalTitle {
			font-size: 1rem;
			color: #2962FF;
			font-weight: 600;
			font-family: "Open Sans", sans-serif;
		}

        .tabs .tab-links .edit {
            display: none;
            cursor: pointer;
            font-size: 14px;
            position: absolute;
            top: 138px;
            right: 40px;
            color: #BDBDBD;
            font-weight: 500;
            font-family: "Open Sans", sans-serif;
            letter-spacing: 0.5px;
        }
        .tabs .tab-links .edit .fa {
            padding-right: 5px;
        }
        .tabs .tab-links .editActive {
            display: inline-block;
        }
        .tabs .tab-links .edit:hover,
        .tabs .tab-links .edit:focus {
            color: #4c4c4c;
        }

        .tabs .tab-links .save {
            padding: 0.3rem 0.5rem;
            background-color: #149dcc;
            color: #fff !important;
            border-color: #149dcc;
            margin: 0;
            right: 35px !important;
        }
        .tabs .tab-links .save:hover {
			background-color: #0d6786;
			border-color: #0d6786;
        }
        .tabs .tab-links .save:focus,
        .tabs .tab-links .save:active {
			background-color: #0d6786;
			border-color: #0d6786;
            box-shadow: 0 0 0 3px #149dcc;
        }
        .tabs .tab-links .cancel {
            padding: 0.3rem 0.5rem;
            background-color: #ff3366;
            border-color: #ff3366;
            color: #fff !important;
            margin: 0;
            right: 110px !important;
        }
        @media (max-width: 767px) {
            .tabs .tab-links .save {
                top: 105px !important;
            }
            .tabs .tab-links .cancel {
                right: 35px !important;
                top: 138px !important;
            }
        }
        .tabs .tab-links .cancel:hover {
            background-color: #e50039;
            border-color: #e50039;
        }
        .tabs .tab-links .cancel:focus,
        .tabs .tab-links .cancel:active {
            background-color: #e50039;
            border-color: #e50039;
            box-shadow: 0 0 0 3px rgba(220,53,69,.5);
        }

		.tab-contents .overviewTab {
			font-family: "Open Sans", sans-serif;
		}
		.tab-contents .overviewTab .muted {
			text-transform: uppercase;
			font-size: 0.6rem;
			letter-spacing: 0.15rem;
			color: #BDBDBD;
			font-weight: bold;
			padding: 18px 0;
		}
		.tab-contents .overviewTab .dataLine {
			padding: 6px 0px;
		}
		.tab-contents .overviewTab .dataLine .dataTitle {
			font-weight: 600;
			display: inline-block;
			width: 100px;
			color: #757575;
		}
		.tab-contents .overviewTab .dataLine .dataContent {
			font-size: 1.1rem;
		}

		.tab-contents .bioTab .bioText {
			font-size: 0.95rem;
			width: 90%;
			color: #757575;
			padding-top: 15px;
		}
        #bioTextareaHelp > p {
            margin: 0;
        }

        /*  Review Tab  */
        .reviewCol {
            margin-bottom: 25px;
        }
        .reviewCol .reviewRating {height: 40px;}

        .reviewCol .reviewRatingDivOne {height: 100%; width: 40px; float: left; border-radius: 2px; background: #2185d5; text-align: center;}
        .reviewCol .reviewRatingDivTwo {height: 100%; width: auto; float: left; padding-left: 15px;}
        .reviewCol .reviewRatingDivTwoOne {height:  25px; width:  auto; line-height: 25px;}
        .reviewCol .reviewRatingDivTwoTwo {height: 15px; line-height: 15px;}

        .reviewCol .ratingNumber {
            line-height: 40px;
            color: white;
            font-weight: bold;
            font-size: 1.1rem;
        }
        .reviewCol .reviewData {
            margin: 15px 0px 0px;
            color: #757575;
            font-size: 1rem;
            line-height: 1.4rem;
        }

        .reviewCol .rating > input {
            display: none;
        }
        .reviewCol .rating > label:before {
            font-size: 1.2rem;
            font-family: FontAwesome;
            display: block;
            content: "\f005";
        }
        .reviewCol .rating > label {
            color: #2185d5;
        }
        .reviewCol .rating > input:checked ~ label {
            color: #ddd;
        }
        .reviewCol .rating > input:checked + label {
            color: #2185d5;
        }
        .pagination .page-link {
            cursor: pointer;
        }
        .pagination .page-link:hover {
            color: black !important;
        }
        .pagination .active .page-link:hover {
            color: white !important;
            cursor: default;
        }
            
        /*  Projects  */
/*
        @media (max-width: 765px){
            #projectSection {
                max-height: 100vh;
                overflow-y: scroll;
            }
        }
        @media (min-width: 768px){
            #projectSection {
                max-height: 70vh;
                overflow-y: scroll;
            }
        }
*/
        #projectSection p {
            margin: 0;
            padding: 0;
        }
        #projectSection .col-12 a {
            text-decoration: none;
            color: #424242;
        }
        #projectSection .col-12 a:hover {
            text-decoration: none;
            color: #424242;
        }
        #projectSection .project {
            background: #F5F5F5;
            padding: 15px 0px;
            border-radius: 3px;
            border: 1.5px solid #E0E0E0;
            cursor: pointer;
        }
        #projectSection .project .projectTitle {
            font-family: "Josefin Slab", "Arvo", monospace;
            font-weight: bold;
            font-size: 1.5rem;
            color: #424242;
            word-wrap: break-word;
        }
        #projectSection .project .projectTitle:hover {
            text-decoration: underline;
        }
        #projectSection .project .projectDesc {
            font-family: "Open Sans", sans-serif;
            font-size: 0.95rem;
            color: #616161;
            word-wrap: break-word;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 4;
            line-height: 1.3rem;
            max-height: 5.2rem;
            overflow: hidden;
        }
        #projectSection .project .projectSubTitle {
            text-transform: uppercase;
            font-weight: bold;
            color: #616161;
        }
        #projectSection .project .projectSubTitle {
            text-transform: uppercase;
            font-weight: 600;
            letter-spacing: 0.1rem;
            color: #616161;
            font-family: "Arvo", monospace;
        }
        
        /*  Wallet  */
        #wallet .balance {
            background: cornflowerblue;
            padding: 15px 20px;
            color: white;
            border-radius: 3px;
            font-family: "Arvo", monospace;
            font-size: 2.5rem;
            text-align: center;
            box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
        }
        #wallet .balance span {
            line-height: 3.75rem;
        }
	</style>

	<!--  Tabs  -->
	<style>
		/*----- Tabs -----*/
		.tabs {
			width:100%;
			display:inline-block;
		}
		/*----- Tab Links -----*/
		/* Clearfix */
		.tab-links:after {
			display:block;
			clear:both;
			content:'';
		}
		.tab-links {
			padding-left: 0;
			margin-bottom: 15px;
		}
		.tab-links li {
			margin: 0px 25px 0px 0px;
			float:left;
			list-style:none;
			background:#fff;
			border-bottom: 1.5px solid transparent;
		}
		.tab-links i {
			padding-right: 10px;
			font-size: 0.8em;
		}
		.tab-links a {
			padding:8px 2px 8px;
			display:inline-block;
			font-size:16px;
			font-weight:600;
			color:#4c4c4c;
			transition:all linear 0.15s;
		}
		.tab-links a:hover {
			text-decoration:none;
		}
		li.active a, li.active a:hover {
			color:#4c4c4c;
			border-bottom: 1.5px solid #2185d5;
		}
		.tab {
			display:none;
		}
		.tab.active {
			display:block;
		}
        
        .tab .imageCard {
            padding-top: 20px;
        }
        
        .tab .imageCard .card-body {
            padding: 0.8rem;
            background: #2185d5;
        }
        .tab .imageCard .card-body > p {
            font-size: 0.9rem;
            color: #fff;
        }
        
        .tab .imageCard .card-footer {
            padding: 1rem;
            background: white;
            font-size: 0.95rem;
            color: #757575;
        }
        .tab .imageCard .card-footer small {
            font-size: 0.95rem;
            color: #757575;
        }
        
        .tab #portfolioTitle {
            border-bottom: 1px solid #757575;
            display: block;
            width: 100%;
            margin: 30px 15px 0;
            text-align: center;
            font-size: 2rem;
            color: #757575;
            font-weight: 600;
            padding-bottom: 10px;
        }
	</style>
    
    <!--  Forms  -->
	<style>
        .cards form .form-control {
            min-height: 0;
            width: 100%;
            padding: .5rem .75rem;
            font-size: 1rem;
            line-height: 1.25;
        }
        .cards form .labelInput,
        .cards .form-group .labelInput {
            font-weight: 600;
            color: #757575;
        }
        .textareaFixedSize {
            resize: none;
            overflow: hidden;
        }

        .cards form .fileColumn {
            padding-bottom: 6px;
        }
        .cards form .form-control-file {
            width: 100%;
        }
        .cards form .btn-file,
        .cards form .btn-submit {
            width: calc(50% - 4px);
            cursor: pointer;
            background: cornflowerblue;
            color: white;
            margin: 0px;
            padding: 0.5rem 0.7rem;
        }
        .cards form .btn-file input[type=file],
        .cards form .btn-submit input[type=submit] {
            display: none;
        }
        .cards form .btn-file {
			background-color: #ff3366;
			border-color: #ff3366;
			color: #ffffff;
        }
        .cards form .btn-file:hover,
        .cards form .btn-file:focus {
			background-color: #e50039;
			border-color: #e50039;
        }
        .cards form .btn-submit {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
        }
        .cards form .btn-submit:hover,
        .cards form .btn-submit:focus {
			background-color: #0d6786;
			border-color: #0d6786;
        }

        .cards .save {
            padding: 0.3rem 0.5rem;
            background-color: #149dcc;
            color: #fff !important;
            border-color: #149dcc;
            margin: 0;
            right: 35px !important;
        }
        .cards .save:hover {
			background-color: #0d6786;
			border-color: #0d6786;
        }
        .cards .save:focus,
        .cards .save:active {
			background-color: #0d6786;
			border-color: #0d6786;
            box-shadow: 0 0 0 3px #149dcc;
        }
        .cards .cancel {
            padding: 0.3rem 0.5rem;
            background-color: #ff3366;
            border-color: #ff3366;
            color: #fff !important;
            margin: 0;
            right: 110px !important;
        }
        @media (max-width: 767px) {
            .cards .cancel {
                right: 35px !important;
                top: 70px !important;
            }
        }
        .cards .cancel:hover {
            background-color: #e50039;
            border-color: #e50039;
        }
        .cards .cancel:focus,
        .cards .cancel:active {
            background-color: #e50039;
            border-color: #e50039;
            box-shadow: 0 0 0 3px rgba(220,53,69,.5);
        }

        .easy-autocomplete {
            width: 100% !important;
        }
    </style>

    <!--  Skill tags  -->
    <style>
        /*  Tags  */
        #desc .tag {
            border-radius: 3px 0 0 3px;
            display: inline-block;
            height: 24px;
            line-height: 24px;
            padding: 0 20px 0 23px;
            position: relative;
            margin: 0 10px 10px 0;
            text-decoration: none;
            -webkit-transition: color 0.2s;
            color: white;
            background-color: #2185d5;
			font-size: 1rem;
        }
        #desc .tag::before {
            background: #fff;
            border-radius: 10px;
            box-shadow: inset 0 1px rgba(0, 0, 0, 0.25);
            content: '';
            height: 6px;
            left: 10px;
            position: absolute;
            width: 6px;
            top: 10px;
        }
        #desc .tag::after {
            background: #fff;
            border-bottom: 12px solid transparent;
            border-left: 10px solid #eee;
            border-top: 12px solid transparent;
            content: '';
            position: absolute;
            right: 0;
            top: 0;
            border-left-color: #2185d5;
        }
    </style>

    <style>
        .skillsDiv span.tag.label.label-info {
            background-color: #5bc0de;
        }
        .skillsDiv span.tag.label {
            display: inline;
            padding: .3em .6em .3em;
            font-size: 75%;
            font-weight: 700;
            color: #fff;
            line-height: 26px;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
        }
        .skillsDiv .bootstrap-tagsinput {
            width: 100%;
            background: #f5f5f5;
        }
        .accordion {
            margin-bottom:-3px;
        }
        .skillsDiv .accordion-group {
            border: none;
        }
        .skillsDiv .twitter-typeahead .tt-query,
        .skillsDiv .twitter-typeahead .tt-hint {
            margin-bottom: 0;
        }
        .skillsDiv .twitter-typeahead .tt-hint
        {
            display: none;
        }
        .skillsDiv .tt-menu {
            position: absolute;
            top: 100%;
            left: 0;
            z-index: 1000;
            display: none;
            float: left;
            min-width: 160px;
            padding: 5px 0;
            margin: 2px 0 0;
            list-style: none;
            font-size: 14px;
            background-color: #fff;
            border: 1px solid #cccccc;
            border: 1px solid rgba(0, 0, 0, 0.15);
            border-radius: 4px;
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
            background-clip: padding-box;
            cursor: pointer;
        }
        .skillsDiv .tt-suggestion {
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: normal;
            line-height: 1.428571429;
            color: #333333;
            white-space: nowrap;
        }
        .skillsDiv .tt-suggestion:hover,
        .skillsDiv .tt-suggestion:focus {
          color: #ffffff;
          text-decoration: none;
          outline: 0;
          background-color: #428bca;
        }
        .skillsDiv .bootstrap-tagsinput .tag [data-role="remove"] {
          margin-left: 8px;
          cursor: pointer;
        }
        .skillsDiv .bootstrap-tagsinput .tag [data-role="remove"]:after {
          content: "x";
          padding: 0px 2px;
        }
        .skillsDiv .bootstrap-tagsinput .tag [data-role="remove"]:hover {
          box-shadow: none;
        }
        .skillsDiv .bootstrap-tagsinput .tag [data-role="remove"]:hover:active {
          box-shadow: none;
        }

		#msg1,
		#msg2,
		#msg3,
		#msg4,
		#msg5,
		#msg6,
        #msg7 {
			height: 18px;
			width: 18px;
		}
    </style>
</head>

<body>

<div class="modal" style="z-index: 1040;">
    <div class="modal-background"></div>
        <div class="modal-content">


            <div class="uploadImg container">
                <p class="modalTitle">Upload Image</p><br>

                <!--  Form for display picture  -->
                <form method="post" action="user/profile/imageUpload" enctype="multipart/form-data">

                    <div class="imgCont">
                        <img alt="No file chosen" id="imgOutput" style="position: relative; left: 50%; transform: translateX(-50%); max-width: 30%; padding-bottom: 20px;"/>
                    </div>

                    <div class="row text-center">

                        <div class="col col-12">
                            <div class="form-group">
                                <input type="file" id="file" class="form-control-file" aria-describedby="dpText" name="myimage" accept="image/*" onchange="loadFile(event)" style="margin: 0 auto; width: 97.19px;">

                                <small id="dpText" class="form-text text-muted"></small>
                            </div>
                        </div>

                        <div class="col col-12">
                            <input type="submit" name="submit_image" value="Upload" class="btn btn-md btn-info" style="padding: 0.5rem 2rem;" disabled>
                        </div>

                        <span id="file_error" style="display: block; text-align: center; width: 100%; padding: 5px 0px 25px; color: red; font-family: 'Open Sans', sans-serif; font-weight: 600;"></span>
                    </div>

                </form>

            </div>

        </div>
    <button class="modal-close is-large" aria-label="close" onclick="hideModal();"></button>
</div>

<script>
    var loadFile = function(event) {
        var output = document.getElementById('imgOutput');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>

<?php if ($this->session->flashdata('msg')) : ?>
	<div class="alertMsg">
		<?= $this->session->flashdata('msg') ?>
	</div>
<?php endif; ?>

<?php if ($this->session->flashdata('validation_error')) : ?>
	<div class="alertMsg">
		<?= $this->session->flashdata('validation_error'); ?>
	</div>
<?php endif; ?>

<section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm">

		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>

		<div class="menu-logo">
			<div class="navbar-brand">

                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!--  Form for user search  -->
            <form id="userSearchForm">

                <div class="input-group pb-3 pb-sm-3 pb-lg-0">
                    <input type="search" class="form-control" placeholder="Search for a user" style="min-height: 0px; padding: 8px 12px; width: 160px; height: 40px; background-color: #fff;"/>

                    <span class="input-group-addon input-group-addon-btn bg-white" style="padding: 0; height: 40px;">
                        <button class="btn px-2" type="button" onclick="searchUser()" style="border: none; background: transparent; cursor: pointer; height: 100%; color: cornflowerblue; margin: 0.4rem 0.2rem;"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                </div>

            </form>

			<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-cash mbr-iconfont mbr-iconfont-btn"></span>My Wallet<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-bookmark mbr-iconfont mbr-iconfont-btn"></span>My Projects<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-sites mbr-iconfont mbr-iconfont-btn"></span>Dashboard<br></a></li>

				<li class="nav-item dropdown">
					<a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
						<span class="profileIconNav"></span><?= $this->session->userdata('username') ?><br></a>

					<div class="dropdown-menu">

						<div class="dropdown">
							<a class="dropdown-item text-white dropdown-toggle display-4" data-toggle="dropdown-submenu" aria-expanded="false">Help</a>

							<div class="dropdown-menu dropdown-submenu">
								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">FAQ</a>

								<a class="dropdown-item text-white display-4" href="#">Contact Us</a>

								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Our Charges</a>
							</div>
						</div>

						<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Post Project</a>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>

					</div>
				</li>
			</ul>
		</div>
	</nav>
</section>

<div class="container wrapper">
	<div class="row" style="box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);">

		<div class="col-sm-4 col-lg-3 is-card is-card-left">

			<div class="image imgWrapper">
				<div class="image">
					<div class="img"></div>
					<span class="changeDP" onclick="showModal();"><i class="fa fa-cog"></i></span>
				</div>
			</div>

			<div class="sectionController">

				<button id="overview" type="button" class="btn btn-info is-pressed" onclick="toggleRightDiv(this.id);">Overview</button>

				<button id="desc" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">Description</button>

				<button id="account" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">Account</button>

				<button id="projects" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">My Projects</button>

				<button id="wallet" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">Wallet</button>

				<button id="reviews" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">Reviews</button>

			</div>

            <a class="btn btn-info btn-outline-primary viewAs" href="<?php echo base_url(); ?>u/<?= $this->session->userdata('username') ;?>">View as Public</a>

			<hr>

			<div class="buttonSection text-center">
                <a class="btn btn-warning" href="<?php echo base_url(); ?>post-project">Post Project</a>

                <a class="btn btn-danger" href="<?php echo base_url(); ?>post-project">Browse Projects</a>
			</div>

		</div>

		<div class="col-sm-8 col-lg-9 is-card is-card-right">

			<div id="overview" class="cards is-shown">

				<p class="titleInfo">
					<span id="username" class="username"><?php if (isset($username)) : ?><?= $username ?><?php endif; ?></span>
					<span><i class="fa fa-map-marker"></i></span>
					<span class="location"><?php if (isset($city)) : ?><?= $city ?><?php endif; ?>, <?php if (isset($country)) : ?><?= $country ?><?php endif; ?></span>
				</p>

				<p class="professionalTitle"><?php if (isset($professional_title)) : ?><?= $professional_title ?><?php endif; ?></p>
				<br>

				<div class="tabs">
					<ul class="tab-links">
						<li class="bioEditContainer">
							<a href="#tab1"><span><i class="fa fa-eye"></i></span>Bio</a>
							<span class="edit" id="bioEdit" onclick="showEditTab(this.id);">
							    <i class="fa fa-pencil"></i>Edit
							</span>

                            <span class="btn cancel edit" id="bioCancel" onclick="cancelForm(this.id);">
                                <i class="fa fa-ban"></i>Cancel
                            </span>
                            <span class="btn save edit" id="bioSubmit" onclick="submitForm(this.id);">
                                <i class="fa fa-save"></i>Save
                            </span>
						</li>

						<li class="active overviewEditContainer">
							<a href="#tab2"><span><i class="fa fa-user"></i></span>Overview</a>
							<span class="edit editActive" id="overviewEdit" onclick="showEditTab(this.id);">
							    <i class="fa fa-pencil"></i>Edit
							</span>

                            <span class="btn cancel edit" id="overviewCancel" onclick="cancelForm(this.id);">
                                <i class="fa fa-ban"></i>Cancel
                            </span>
                            <span class="btn save edit" id="overviewSubmit" onclick="submitForm(this.id);">
                                <i class="fa fa-save"></i>Save
                            </span>
						</li>
					</ul>

					<div class="tab-contents">

						<div id="tab1" class="tab bioTab">
							<p class="bioText">
								<?php if (isset($bio)) : ?><?= $bio ?><?php endif; ?>
							</p>
						</div>

						<div id="bioEditTab" class="tab bioTab">

                            <!--  Form for user bio  -->
						    <form action="<?php echo base_url();?>profile/updateBioProfile" method="post" id="bioSubmitForm">
						        <div class="form-group">
                                    <small id="bioTextareaHelp" class="form-text" style="color: #dc3545 !important; font-size: 0.9rem; padding-bottom: 0.5rem;"></small>

                                    <textarea aria-describedby="bioTextareaHelp" id="bioTextarea" class="form-control textareaFixedSize" rows="20" name="bio" placeholder="Write a brief description about yourself to increase your chances of getting hired!"><?php if (isset($bio)) : ?><?= $bio ?><?php endif; ?></textarea>
                                </div>
						    </form>
						</div>

						<div id="tab2" class="tab overviewTab active">

							<p class="muted">Contact Information</p>

							<p class="dataLine">
								<span class="dataTitle">Name:</span>
								<span class="dataContent"><?php if (isset($name)) : ?><?= $name ?><?php endif; ?></span>
							</p>
							<p class="dataLine">
								<span class="dataTitle">Address:</span>
								<span class="dataContent">
									<?php if (isset($address1)) : ?>
										<?= $address1 ?>,
									<?php endif; ?>
									<?php if (isset($address2)) : ?>
										<?= $address2 ?>.
									<?php endif; ?>
								</span>
							</p>
							<p class="dataLine">
								<span class="dataTitle">Phone:</span>
								<span class="dataContent"><?php if (isset($phone)) : ?><?= $phone ?><?php endif; ?></span>
							</p>
							<p class="dataLine">
								<span class="dataTitle">E-mail:</span>
								<span class="dataContent"><?php if (isset($email)) : ?><?= $email ?><?php endif; ?></span>
							</p>

							<p class="muted">Personal Information</p>

							<p class="dataLine">
								<span class="dataTitle">Gender:</span>
								<span class="dataContent"><?php if (isset($gender)) : ?><?= $gender ?><?php endif; ?></span>
							</p>
							
							<div class="row">
                                <?php
                                    if( ($portfolio_link1 != "") || ($portfolio_link2 != "") || ($portfolio_link3 != "") || ($portfolio_link4 != "") )
                                        echo '<p id="portfolioTitle">Portfolio</p>';
                                ?>
                                
                                <?php if ( $portfolio_link1 != "") : ?><?= '<div class="imageCard col col-12 col-md-6 col-lg-3"><div class="card text-center"><img class="card-img-top" src="' ?><?= base_url().$portfolio_pic1; ?><?= '" alt="Portfolio Image" onError="showErrorImage(this);"><a class="card-body" target="_blank" href="http://' ?><?= $portfolio_link1 ?><?= '"><p class="card-text">' ?><?= $portfolio_link1 ?><?= '</p></a><div class="card-footer text-center"><small">' ?><?= $portfolio_desc1 ?><?= '</small></div></div></div>' ?><?php endif; ?>
                                
                                <?php if ( $portfolio_link2 != "") : ?><?= '<div class="imageCard col col-12 col-md-6 col-lg-3"><div class="card text-center"><img class="card-img-top" src="' ?><?= base_url().$portfolio_pic2; ?><?= '" alt="Portfolio Image" onError="showErrorImage(this);"><a class="card-body" target="_blank" href="http://' ?><?= $portfolio_link2 ?><?= '"><p class="card-text">' ?><?= $portfolio_link2 ?><?= '</p></a><div class="card-footer text-center"><small">' ?><?= $portfolio_desc2 ?><?= '</small></div></div></div>' ?><?php endif; ?>
                                
                                <?php if ( $portfolio_link3 != "") : ?><?= '<div class="imageCard col col-12 col-md-6 col-lg-3"><div class="card text-center"><img class="card-img-top" src="' ?><?= base_url().$portfolio_pic3; ?><?= '" alt="Portfolio Image" onError="showErrorImage(this);"><a class="card-body" target="_blank" href="http://' ?><?= $portfolio_link2 ?><?= '"><p class="card-text">' ?><?= $portfolio_link3 ?><?= '</p></a><div class="card-footer text-center"><small">' ?><?= $portfolio_desc3 ?><?= '</small></div></div></div>' ?><?php endif; ?>
                                
                                <?php if ( $portfolio_link4 != "") : ?><?= '<div class="imageCard col col-12 col-md-6 col-lg-3"><div class="card text-center"><img class="card-img-top" src="' ?><?= base_url().$portfolio_pic4; ?><?= '" alt="Portfolio Image" onError="showErrorImage(this);"><a class="card-body" target="_blank" href="http://' ?><?= $portfolio_link4 ?><?= '"><p class="card-text">' ?><?= $portfolio_link4 ?><?= '</p></a><div class="card-footer text-center"><small">' ?><?= $portfolio_desc4 ?><?= '</small></div></div></div>' ?><?php endif; ?>
							    
							</div>
                            
						</div>

						<div id="overviewEditTab" class="tab overviewTab">

                            <!--  Form for overview details  -->
                            <form action="<?php echo base_url();?>profile/updateOverviewProfile" method="post" autocomplete="off" id="overviewSubmitForm">

                                <p class="muted">Contact Information</p>

                                <div class="form-group row">
                                    <label for="name" class="col-md-3 col-form-label labelInput">Name:</label>

                                    <div class="col-md-9">
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter your name" value="<?php if (isset($name)) : ?><?= $name ?><?php endif; ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="add1" class="col-md-3 col-form-label labelInput">Address:</label>

                                    <div class="col-md-9">
                                        <textarea type="text" name="add1" class="form-control textareaFixedSize" id="add1" placeholder="Enter your address" rows="2"><?php if (isset($address1)) : ?><?= $address1 ?><?php endif; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="add2" class="col-md-3 col-form-label labelInput">Locality:</label>

                                    <div class="col-md-9">
                                        <textarea type="text" name="add2" class="form-control textareaFixedSize" id="add2" placeholder="Enter your locality" rows="2"><?php if (isset($address2)) : ?><?= $address2 ?><?php endif; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-md-3 col-form-label labelInput">Phone:</label>

                                    <div class="col-md-9">
                                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter your phone number" value="<?php if (isset($phone)) : ?><?= $phone ?><?php endif; ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-3 col-form-label labelInput">E-mail:</label>

                                    <div class="col-md-9">
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter your e-mail" value="<?php if (isset($email)) : ?><?= $email ?><?php endif; ?>" disabled>
                                    </div>
                                </div>

							    <p class="muted">Personal Information</p>

                                <div class="form-group row">
                                    <label for="gender" class="col-md-3 col-form-label labelInput">Gender:</label>

                                    <div class="col-md-9">
                                        <select class="form-control" id="gender" name="gender">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>

                            </form>
                            
                            <div class="row">
                                <p id="portfolioTitle" style="margin-bottom: 20px;">
                                   Portfolio 
                                </p>  
                                
                                <!--  Form for portfolio 1 -->
                                <form action="" id="portfolio_form_1" method="post" class="form-group col col-12 col-lg-6" enctype="multipart/form-data">
                                    
                                    <div class="row">
                                        <label class="col-12 labelInput">Portfolio #1:</label>
                                        
                                        <div class="col-12 fileColumn">
                                            <label class="btn btn-file">
                                                Browse <input type="file" id="portfolio1" name="portfolioPic1" accept="file_extension|.jpg|.jpeg">
                                            </label>

                                            <label id="submitButtonPort1" class="btn btn-submit disabled">
                                                Upload <input type="submit" disabled>
                                            </label>
                                        </div>
                                        
                                        <small class="col-12 labelData pb-1" id="fileNamePort1"></small>

                                        <div class="col-12 fileColumn">
                                            <input type="text" id="portfolioLink1" name="portfolioLink1" class="form-control" placeholder="Link to your portfolio" value="<?php if (isset($portfolio_link1)) : ?><?= $portfolio_link1 ?><?php endif; ?>">
                                        </div>

                                        <div class="col-12 fileColumn">
                                            <textarea type="text" name="portfolioDesc1" class="form-control textareaFixedSize"  placeholder="Describe your portfolio" rows="3"><?php if (isset($portfolio_desc1)) : ?><?= $portfolio_desc1 ?><?php endif; ?></textarea>
                                        </div>
                                        
										<div class="AjaxDiv4" style="padding: 0 15px;">
											<span><img src="<?php echo base_url();?>assets/Images/success.svg" id="msg4"></span>
											<span id="upload_status_4">Successfully uploaded</span>
										</div>
                                        
                                    </div>
                                </form>
                                
                                <!--  Form for portfolio 2 -->
                                <form action="" id="portfolio_form_2" method="post" class="form-group col col-12 col-lg-6" enctype="multipart/form-data">
                                    
                                    <div class="row">
                                        <label class="col-12 labelInput">Portfolio #2:</label>

                                        <div class="col-12 fileColumn">
                                            <label class="btn btn-file">
                                                Browse <input type="file" id="portfolio2" name="portfolioPic2" accept="file_extension|.jpg|.jpeg">
                                            </label>

                                            <label id="submitButtonPort2" class="btn btn-submit disabled">
                                                Upload <input type="submit" disabled>
                                            </label>
                                        </div>
                                        
                                        <small class="col-12 labelData pb-1" id="fileNamePort2"></small>

                                        <div class="col-12 fileColumn">
                                            <input type="text" id="portfolioLink2" name="portfolioLink2" class="form-control" placeholder="Link to your portfolio" value="<?php if (isset($portfolio_link2)) : ?><?= $portfolio_link2 ?><?php endif; ?>">
                                        </div>

                                        <div class="col-12 fileColumn">
                                            <textarea type="text" name="portfolioDesc2" class="form-control textareaFixedSize"  placeholder="Describe your portfolio" rows="3"><?php if (isset($portfolio_desc2)) : ?><?= $portfolio_desc2 ?><?php endif; ?></textarea>
                                        </div>
										<div class="AjaxDiv5" style="padding: 0 15px;">
											<span><img src="" id="msg5"></span>
											<span id="upload_status_5"></span>
										</div>
                                        
                                    </div>                                    
                                </form>
                                
                                <!--  Form for portfolio 3 -->
                                <form action="" id="portfolio_form_3" method="post" class="form-group col col-12 col-lg-6" enctype="multipart/form-data">
                                    
                                    <div class="row">
                                        <label class="col-12 labelInput">Portfolio #3:</label>

                                        <div class="col-12 fileColumn">
                                            <label class="btn btn-file">
                                                Browse <input type="file" id="portfolio3" name="portfolioPic3" accept="file_extension|.jpg|.jpeg">
                                            </label>

                                            <label id="submitButtonPort3" class="btn btn-submit disabled">
                                                Upload <input type="submit" disabled>
                                            </label>
                                        </div>
                                        
                                        <small class="col-12 labelData pb-1" id="fileNamePort3"></small>

                                        <div class="col-12 fileColumn">
                                            <input type="text" id="portfolioLink3" name="portfolioLink3" class="form-control" placeholder="Link to your portfolio" value="<?php if (isset($portfolio_link3)) : ?><?= $portfolio_link3 ?><?php endif; ?>">
                                        </div>

                                        <div class="col-12 fileColumn">
                                            <textarea type="text" name="portfolioDesc3" class="form-control textareaFixedSize"  placeholder="Describe your portfolio" rows="3"><?php if (isset($portfolio_desc3)) : ?><?= $portfolio_desc3 ?><?php endif; ?></textarea>
                                        </div>
										<div class="AjaxDiv6" style="padding: 0 15px;">
											<span><img src="" id="msg6"></span>
											<span id="upload_status_6"></span>
										</div>
                                        
                                    </div>                                    
                                </form>
                                
                                <!--  Form for portfolio 4 -->
                                <form action="" id="portfolio_form_4" method="post" class="form-group col col-12 col-lg-6" enctype="multipart/form-data">
                                    
                                    <div class="row">
                                        <label class="col-12 labelInput">Portfolio #4:</label>

                                        <div class="col-12 fileColumn">
                                            <label class="btn btn-file">
                                                Browse <input type="file" id="portfolio4"  name="portfolioPic4" accept="file_extension|.jpg|.jpeg">
                                            </label>

                                            <label id="submitButtonPort4" class="btn btn-submit disabled">
                                                Upload <input type="submit" disabled>
                                            </label>
                                        </div>
                                        
                                        <small class="col-12 labelData pb-1" id="fileNamePort4"></small>

                                        <div class="col-12 fileColumn">
                                            <input type="text" id="portfolioLink4" name="portfolioLink4" class="form-control" placeholder="Link to your portfolio" value="<?php if (isset($portfolio_link4)) : ?><?= $portfolio_link4 ?><?php endif; ?>">
                                        </div>

                                        <div class="col-12 fileColumn">
                                            <textarea type="text" name="portfolioDesc4" class="form-control textareaFixedSize"  placeholder="Describe your portfolio" rows="3"><?php if (isset($portfolio_desc4)) : ?><?= $portfolio_desc4 ?><?php endif; ?></textarea>
                                        </div>
										<div class="AjaxDiv7" style="padding: 0 15px;">
											<span><img src="" id="msg7"></span>
											<span id="upload_status_7"></span>
										</div>
                                        
                                    </div>                                    
                                </form>
                                
                            </div>
                    	</div>

					</div>
				</div>

			</div>

			<div id="descEditWrapper" class="cards container">

				<div class="title">
					<p>Description</p>
					<span class="titleUnderline"></span>

					<span class="btn cancel edit" id="descCancel" onclick="cancelForm(this.id);">
					    <i class="fa fa-ban"></i>Cancel
					</span>
					<span class="btn save edit" id="descSubmit" onclick="submitForm(this.id);">
					    <i class="fa fa-save"></i>Save
					</span>
				</div>

                <!--  Form for user description  -->
                <form action="<?php echo base_url();?>profile/updateDescriptionProfile" method="post" autocomplete="off" id="descSubmitForm" onsubmit="return skillFunc()">

                    <div class="form-group row">
						<label for="country" class="col-md-3 col-form-label labelInput">Country:</label>

						<div class="col-md-9">
                            <select id="country" name="country" class="form-control"></select>
						</div>
					</div>

                    <div class="form-group row" id ="stateDummy">
						<label for="stateFake" class="col-md-3 col-form-label labelInput">State:</label>

						<div class="col-md-9">
                            <select id="stateFake" class="form-control" disabled></select>
						</div>
					</div>

                    <div class="form-group row" id ="stateReal">
						<label for="state" class="col-md-3 col-form-label labelInput">State:</label>

						<div class="col-md-9">
                            <select id="state" name="state" class="form-control"></select>
						</div>
					</div>

                    <div class="form-group row">
                        <label for="city" class="col-md-3 col-form-label labelInput">City:</label>

                        <div class="col-md-9">
                            <input type="text" name="city" class="form-control" id="city" placeholder="Enter your city" value="<?php if (isset($city)) : ?><?= $city ?><?php endif; ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="pincode" class="col-md-3 col-form-label labelInput">Pin-code:</label>

                        <div class="col-md-9">
                            <input type="text" name="pincode" class="form-control" id="pincode" value="<?php if (isset($pincode)) : ?><?= $pincode ?><?php endif; ?>" placeholder="Enter your pincode">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="skypeId" class="col-md-3 col-form-label labelInput">Skype:</label>

                        <div class="col-md-9">
                            <input type="email" name="skypeId" class="form-control" id="skypeId" value="<?php if (isset($skypeId)) : ?><?= $skypeId ?><?php endif; ?>" placeholder="Enter your skype id">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="professionalTitle" class="col-md-3 col-form-label labelInput">Title:</label>

                        <div class="col-md-9">
                            <input type="text" name="professionalTitle" class="form-control" id="professionalTitle" value="<?php if (isset($professional_title)) : ?><?= $professional_title ?><?php endif; ?>" placeholder="Enter your professional title">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="qualifications" class="col-md-3 col-form-label labelInput">Qualifications:</label>

                        <div class="col-md-9">
                            <input type="text" name="qualifications" class="form-control" id="qualifications" value="<?php if (isset($qualification)) : ?><?= $qualification ?><?php endif; ?>" placeholder="Enter your qualifications">
                        </div>
                    </div>

                    <div class="form-group row" style="display: none;">
                        <label for="skillsReal" class="col-md-3 col-form-label labelInput">Skills:</label>

                        <div class="col-md-9">
                            <input type="text" name="skills" class="form-control" id="skillsReal" value="<?php if (isset($skills)) : ?><?= $skills ?><?php endif; ?>" placeholder="Enter your skills">
                        </div>
                    </div>

                    <div class="form-group row skillsDiv">
                        <label for="skills" class="col-md-3 col-form-label labelInput">Skills:</label>

                        <div class="col-md-9">
                            <small id="skillsHelp" class="form-text text-muted" style="font-size: 1.1rem; margin-bottom: 0.5rem;"></small>

                            <input type="text" class="form-control" id="skills" placeholder="Enter atmost 8 skills" aria-describedby="skillsHelp" data-role="tagsinput" value="<?php if (isset($skills)) : ?><?= $skills ?><?php endif; ?>">
                        </div>
                    </div>

                </form>
                
                <div class="row">
                    <div class="col col-md-9 offset-md-3">
                        <p style="font-size: 0.8rem; color: #ff3366; font-weight: 500; margin-bottom: 0.5rem;">*Maximum file size is 2 MB. Only pdf and image files can be uploaded!</p>
                    </div>
                </div>
                
                <!--  Div for certificate 1 -->
                <div class="form-group row">
                    <label class="col-md-3 col-form-label labelInput">Certificate #1:</label>

                    <!--  Form for certificate 1  -->
                    <form action="" id="upload_form_1" method="post" enctype="multipart/form-data" class="col-md-9">
                        <div class="row">

                            <div class="col-12 col-lg-5 pr-lg-0 fileColumn">
                                <label class="btn btn-file">
                                    Browse <input type="file" id="certification1" name="certificationFile1" accept="file_extension|.pdf|.jpg|.jpeg">
                                </label>

                                <label id="submitButtonForm1" class="btn btn-submit disabled">
                                    Upload <input type="submit" disabled>
                                </label>
                            </div>

                            <div class="col-12 col-lg-7 fileColumn">
                                <textarea type="text" class="form-control textareaFixedSize"  placeholder="Describe your certificate" name="certification1" rows="3"><?php if (isset($certification_1)) : ?><?= $certification_1 ?><?php endif; ?></textarea>
                            </div>
                            
                            <div class="AjaxDiv1" style="padding: 0 15px;">
                                <span><img src="" id="msg1"></span>
                                <span id="upload_status_1"></span>
                            </div>
                        </div>
                    </form>
                </div>

                <!--  Div for certificate 2 -->
                <div class="form-group row">
                    <label class="col-md-3 col-form-label labelInput">Certificate #2:</label>

                    <!--  Form for certificate 2  -->
                    <form action="" id="upload_form_2" method="post" enctype="multipart/form-data" class="col-md-9">
                        <div class="row">

                            <div class="col-12 col-lg-5 pr-lg-0 fileColumn">
                                <label class="btn btn-file">
                                    Browse <input type="file" id="certification2" name="certificationFile2" accept="file_extension|.pdf|.jpg|.jpeg">
                                </label>

                                <label id="submitButtonForm2" class="btn btn-submit disabled">
                                    Upload <input type="submit" disabled>
                                </label>
                            </div>

                            <div class="col-12 col-lg-7 fileColumn">
                                <textarea type="text" class="form-control textareaFixedSize" placeholder="Describe your certificate" name="certification2" rows="3"><?php if (isset($certification_2)) : ?><?= $certification_2 ?><?php endif; ?></textarea>
                            </div>
                            
                            <div class="AjaxDiv2" style="padding: 0 15px;">
                                <span><img src="" id="msg2"></span>
                                <span id="upload_status_2"></span>
                            </div>
                        </div>
                    </form>
                </div>

                <!--  Div for certificate 3 -->
                <div class="form-group row">
                    <label class="col-md-3 col-form-label labelInput">Certificate #3:</label>

                    <!--  Form for certificate 3  -->
                    <form action="" id="upload_form_3" method="post" enctype="multipart/form-data" class="col-md-9">
                        <div class="row">

                            <div class="col-12 col-lg-5 pr-lg-0 fileColumn">
                                <label class="btn btn-file">
                                    Browse <input type="file" id="certification3" name="certificationFile3" accept="file_extension|.pdf|.jpg|.jpeg">
                                </label>

                                <label id="submitButtonForm3" class="btn btn-submit disabled">
                                    Upload <input type="submit" disabled>
                                </label>
                            </div>

                            <div class="col-12 col-lg-7 fileColumn">
                                <textarea type="text" class="form-control textareaFixedSize" placeholder="Describe your certificate" name="certification3" rows="3"><?php if (isset($certification_3)) : ?><?= $certification_3 ?><?php endif; ?></textarea>
                            </div>
                            
                            <div class="AjaxDiv3" style="padding: 0 15px;">
                                <span><img src="" id="msg3"></span>
                                <span id="upload_status_3"></span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

			<div id="desc" class="cards">

				<div class="title">
					<p>Description</p>
					<span class="titleUnderline"></span>

					<span class="edit" id="descEdit" onclick="showEditPane(this.id);">
					    <i class="fa fa-pencil"></i>Edit
					</span>
				</div>

				<div class="textContent">
					<span class="textTitle">City: </span>
					<span class="textData"><?php if (isset($city)) : ?><?= $city ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">State: </span>
					<span class="textData"><?php if (isset($state)) : ?><?= $state ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Country: </span>
					<span class="textData"><?php if (isset($country)) : ?><?= $country ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Pin code: </span>
					<span class="textData"><?php if (isset($pincode)) : ?><?= $pincode ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Skype id: </span>
					<span class="textData"><?php if (isset($skypeId)) : ?><?= $skypeId ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Professional title: </span>
					<span class="textData"><?php if (isset($professional_title)) : ?><?= $professional_title ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Qualifications: </span>
					<span class="textData"><?php if (isset($qualification)) : ?><?= $qualification ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Skills: </span>

                    <span class="dataContent">
                        <?php if (isset($skills)) : ?>
                            <?php
                                $MyArray = explode(',', $skills);
                                foreach($MyArray as $skill) {
                                    if( $skill != "" )
                                        echo "<span class=\"tag\">$skill</span>";
                                }
                            ?>
                        <?php endif; ?>
                    </span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Certification: </span>
                    <?php if(($certification_1 == "") && ($certification_2 == "") && ($certification_3 == "")) : ?><?= '<span class="textData">No certificate uploaded!</span>' ?><?php endif; ?>
				</div>

                <p class="dataLine">
                    <div class="dataContent row">

                        <?php if($certification_1 != "") : ?><?= '<div class="certificateFile col col-4 col-sm-3 col-md-3  col-lg-2 offset-lg-2 offset-md-3 text-center" id="pdfFile1" onclick="downloadPdf(this.id);" style="cursor: pointer;"><i class="fa fa-file-text-o" style="display: block; font-size: 5rem; padding-bottom: 0.5rem; color: #2185d5;"></i><span style="display: block; font-size: 0.8rem; padding-bottom: 0.8rem; color: #757575; font-weight: 500;">'?><?= $certification_1 ?><?= '</span></div>' ?><?php endif; ?>

                        <?php if($certification_2 != "") : ?><?= '<div class="certificateFile col col-4 col-sm-3 col-md-3 col-lg-2 text-center" id="pdfFile2" onclick="downloadPdf(this.id);" style="cursor: pointer;"><i class="fa fa-file-text-o" style="display: block; font-size: 5rem; padding-bottom: 0.5rem; color: #2185d5;"></i><span style="display: block; font-size: 0.8rem; padding-bottom: 0.8rem; color: #757575; font-weight: 500;">'?><?= $certification_2 ?><?= '</span></div>' ?><?php endif; ?>

                        <?php if($certification_3 != "") : ?><?= '<div class="certificateFile col col-4 col-sm-3 col-md-3 col-lg-2 text-center" id="pdfFile3" onclick="downloadPdf(this.id);" style="cursor: pointer;"><i class="fa fa-file-text-o" style="display: block; font-size: 5rem; padding-bottom: 0.5rem; color: #2185d5;"></i><span style="display: block; font-size: 0.8rem; padding-bottom: 0.8rem; color: #757575; font-weight: 500;">'?><?= $certification_3 ?><?= '</span></div>' ?><?php endif; ?>

                    </div>
                </p>

			</div>

			<div id="accountEditWrapper" class="cards container">

				<div class="title">
					<p>Account</p>
					<span class="titleUnderline"></span>

					<span class="btn cancel edit" id="accountCancel" onclick="cancelForm(this.id);">
					    <i class="fa fa-ban"></i>Cancel
					</span>
					<span class="btn save edit" id="accountSubmit" onclick="submitForm(this.id);" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing Order">
					    <i class="fa fa-save"></i>Save
					</span>
				</div>

                <!--  Form for bank account details  -->
                <form action="<?php echo base_url();?>profile/updateKYCProfile" method="post" autocomplete="off" id="accountSubmitForm">

                    <div class="form-group row">
                        <label for="accountName" class="col-md-3 col-form-label labelInput">Account name:</label>

                        <div class="col-md-9">
                            <input type="text" name="accountName" class="form-control" id="accountName" value="<?php if (isset($bank_account_name)) : ?><?= $bank_account_name ?><?php endif; ?>" placeholder="Enter your account name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="accountNumber" class="col-md-3 col-form-label labelInput">Account number:</label>

                        <div class="col-md-9">
                            <input type="text" name="accountNumber" class="form-control" id="accountNumber" value="<?php if (isset($account_number)) : ?><?= $account_number ?><?php endif; ?>" placeholder="Enter your account number">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="bankName" class="col-md-3 col-form-label labelInput">Bank name:</label>

                        <div class="col-md-9">
                            <input type="text" name="bankName" class="form-control" id="bankName" value="<?php if (isset($bank_name)) : ?><?= $bank_name ?><?php endif; ?>" placeholder="Enter your bank name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="bankBranch" class="col-md-3 col-form-label labelInput">Branch:</label>

                        <div class="col-md-9">
                            <input type="text" name="bankBranch" class="form-control" id="bankBranch" value="<?php if (isset($bank_branch)) : ?><?= $bank_branch ?><?php endif; ?>" placeholder="Enter your bank's branch">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="ifsc" class="col-md-3 col-form-label labelInput">IFSC:</label>

                        <div class="col-md-9">
                            <input type="text" name="ifsc" class="form-control" id="ifsc" value="<?php if (isset($ifsc)) : ?><?= $ifsc ?><?php endif; ?>" placeholder="Enter your bank's IFSC code">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="bankAdd" class="col-md-3 col-form-label labelInput">Address:</label>

                        <div class="col-md-9">
                            <textarea type="text" name="bankAdd" class="form-control textareaFixedSize" id="bankAdd" placeholder="Enter your bank's address" rows="3"><?php if (isset($bank_address)) : ?><?= $bank_address ?><?php endif; ?></textarea>
                        </div>
                    </div>

					<div class="form-group row">
						<label for="payment_method" class="col-md-3 col-form-label labelInput">Payment method:</label>

						<div class="col-md-9">
                            <select class="form-control" id="payment_method" name="payment_method" >
                              <option selected disabled><?php if (isset($payment_method)) : ?><?= $payment_method ?><?php endif; ?><?php if (!isset($payment_method)) : ?>Select payment method<?php endif; ?></option>

                              <option value="IMPS">IMPS</option>
                              <option value="NEFT">NEFT</option>
                              <option value="UPI">UPI</option>
                              <option value="Cheque">Cheque</option>
                            </select>
						</div>
					</div>

                </form>
            </div>

			<div id="account" class="cards">

				<div class="title">
					<p>Account</p>
					<span class="titleUnderline"></span>

					<span class="edit" id="accountEdit" onclick="showEditPane(this.id);">
					    <i class="fa fa-pencil"></i>Edit
					</span>
				</div>

				<div class="textContent">
					<span class="textTitle">Account name: </span>
					<span class="textData"><?php if (isset($bank_account_name)) : ?><?= $bank_account_name ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Account number: </span>
					<span class="textData"><?php if (isset($account_number)) : ?><?= $account_number ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Bank name: </span>
					<span class="textData"><?php if (isset($bank_name)) : ?><?= $bank_name ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Branch: </span>
					<span class="textData"><?php if (isset($bank_branch)) : ?><?= $bank_branch ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">IFSC: </span>
					<span class="textData"><?php if (isset($ifsc)) : ?><?= $ifsc ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Bank address: </span>
					<span class="textData"><?php if (isset($bank_address)) : ?><?= $bank_address ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">
				<div class="textContent">
					<span class="textTitle">Payment method: </span>
					<span class="textData"><?php if (isset($payment_method)) : ?><?= $payment_method ?><?php endif; ?></span>
				</div>
                <hr class="lineBreak">

			</div>

			<div id="projects" class="cards">

				<div class="title">
					<p>My Projects</p>
					<span class="titleUnderline"></span>
				</div>
				
				<div id="projectSection" class="row projectWrapper pr-3 pl-3">
                    
                    <!--  Project skeleton  -->
                    
				</div>
			</div>

			<div id="wallet" class="cards">

				<div class="title">
					<p>Wallet</p>
					<span class="titleUnderline"></span>
				</div>
			</div>

			<div id="reviews" class="cards">

				<div class="title">
					<p>Reviews</p>
					<span class="titleUnderline"></span>
				</div>
                
                <!--  Div for reviews  -->
				<div id="reviewData" class="row" style="margin-top: 40px;"></div>
				
                <?php if ( $count != "" ) : ?><?php $pages = ceil( ((int)$count) / 4 ); ?><?php endif; ?>
                
                <?php if( $pages > 1 ) : ?><?= '<ul class="pagination" id="pagination">'; ?>
                    <?php
                        for($i=0; $i<$pages; $i++) {
                            echo '<li class="page-item';
                            if( $i == 0)
                                echo ' active';
                            echo '"><a id="pageLink'.($i+1).'" class="page-link" onclick="loadReview('.($i*4).')">'.($i+1).'</a></li>';
                        }
                    ?>
                <?= '</ul>' ?>
                <?php endif; ?>
                            
                <?php
                    if( $count == "0" ) {
                        echo "<span style='font-weight: 600; color: #757575;'>No reviews posted yet!</span>";
                    }
                ?>

			</div>

		</div>

	</div>
</div>

<script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>

<script src="<?php echo base_url();?>assets/js/tagsinput.js"></script>
<script src="<?php echo base_url();?>assets/js/typeahead.bundle.js"></script>

<script>

	// ------------- Load Reviews Ajax ----------- //
	function loadReview(offset) {
        var toActivate = (parseInt(offset))/4 + 1;
        if( $("#pagination #pageLink" + toActivate).parent().hasClass("active") )
            return false;
        $("#pagination li").removeClass("active");
        $("#pagination #pageLink" + toActivate).parent().addClass("active");
        
		var pass = parseInt(offset);
		var username = $('#username').text();
		$.ajax ({
			url: "<?php echo base_url(); ?>profile/loadReviews",
			method: "post",
			data: {offset: pass, username: username},
			success: function (data) {
				$("#reviewData").html("");
				$("#reviewData").html(data);
			}
		});
	}
    
	// ------------- Bio Edit Ajax ----------- //

    $('#bioTextarea').bind('input',function () {
        var bio = $('#bioTextarea').val();
        if (bio == '') {
            $('#bioTextareaHelp').html("Bio cannot be empty!");
            $('#bioTextareaHelp').css("color", "#f77d0e");
            $('#bioTextarea').css("border-color", "#f77d0e");
        } else {
            $.ajax({
                url: "<?php echo base_url(); ?>profile/check_bio_length",
                method: "post",
                data: {bio: bio},
                success: function (data) {
                    $('#bioTextareaHelp').html(data);
                    if (data == "Bio content acceptable") {
                        $('#bioTextareaHelp').css("color", "green");
                        $('#bioTextarea').css("border-color", "green");
                    } else {
                        $('#bioTextareaHelp').css("color", "red");
                        $('#bioTextarea').css("border-color", "red");
                    }
                }
            });
        }
    });
    
	$(document).ready(function () {
		var pass = 0;
		var username = $('#username').text();
		$.ajax ({
			url: "<?php echo base_url(); ?>profile/loadReviews",
			method: "post",
			data: {offset: pass, username: username},
			success: function (data) {
				$("#reviewData").html("");
				$("#reviewData").html(data);
			}
		});
	});
    
	$(document).ready(function(){
        $(".AjaxDiv1").hide();
        $(".AjaxDiv2").hide();
        $(".AjaxDiv3").hide();
        $(".AjaxDiv4").hide();
        $(".AjaxDiv5").hide();
        $(".AjaxDiv6").hide();
        $(".AjaxDiv7").hide();

		// ------------- Certificate Ajax ----------- //
		$('#upload_form_1').on('submit', function(e){
			e.preventDefault();
			if($('#certification1').val() == '')
			{
				$('#upload_status_1').html("Please select a file!");
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>profile/updateCertificate1",
					method:"post",
					data:new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success:function(data) {
						$('#msg1').html(data);
                        
						if(data == "Success"){
							$("img#msg1").attr("src", "<?php echo base_url();?>assets/Images/success.svg");
							$("#upload_status_1").html("Successfully uploaded!");
							$("#upload_status_1").css("color", "#5cb85c");
						} else {
							$("img#msg1").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_1").html("Failed to upload!");
							$("#upload_status_1").css("color", "#d9534f");
						}
                        $(".AjaxDiv1").fadeIn(400);
                        setTimeout('$(".AjaxDiv1").fadeOut(400)', 5000);
					}
				});
			}
		});

		$('#upload_form_2').on('submit', function(e){
			e.preventDefault();
			if($('#certification2').val() == '')
			{
				$('#upload_status_2').html("Please select a file!");
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>profile/updateCertificate2",
					method:"post",
					data:new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success:function(data){
						$('#msg2').html(data);
                        
						if(data == "Success"){
							$("img#msg2").attr("src", "<?php echo base_url();?>assets/Images/success.svg");
							$("#upload_status_2").html("Successfully uploaded!");
							$("#upload_status_2").css("color", "#5cb85c");
						} else {
							$("img#msg2").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_2").html("Failed to upload!");
							$("#upload_status_2").css("color", "#d9534f");
						}
                        $(".AjaxDiv2").fadeIn(400);
                        setTimeout('$(".AjaxDiv2").fadeOut(400)', 5000);
					}
				});
			}
		});

		$('#upload_form_3').on('submit', function(e){
			e.preventDefault();
			if($('#certification3').val() == '')
			{
				$('#upload_status_3').html("Please select a file!");
				$('#upload_status_3').css("color", "#999999");
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>profile/updateCertificate3",
					method:"post",
					data:new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success:function(data){
						$('#msg3').html(data);
                        
						if(data == "Success"){
							$("img#msg3").attr("src", "<?php echo base_url();?>assets/Images/success.svg");
							$("#upload_status_3").html("Successfully uploaded!");
							$("#upload_status_3").css("color", "#5cb85c");
						} else {
							$("img#msg3").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_3").html("Failed to upload!");
							$("#upload_status_3").css("color", "#d9534f");
						}
                        $(".AjaxDiv3").fadeIn(400);
                        setTimeout('$(".AjaxDiv3").fadeOut(400)', 5000);
					}
				});
			}
		});

		// ------------- Portfolio Ajax ----------- //
		$('#portfolio_form_1').on('submit', function(e){
			e.preventDefault();
			if($('#portfolio1').val() == '')
			{
				$('#upload_status_4').html("Please select a file!");
				$('#upload_status_4').css("color", "#999999");
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>profile/portfolio1",
					method:"post",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success:function(data){

						if(data == "Success"){
							$("img#msg4").attr("src", "<?php echo base_url();?>assets/Images/success.svg");
							$("#upload_status_4").html("Successfully uploaded!");
							$("#upload_status_4").css("color", "#5cb85c");
						}
						else if(data == "Failed") {
							$("img#msg4").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_4").html("Failed to upload!");
							$("#upload_status_4").css("color", "#d9534f");
						}
						else {
							$("img#msg4").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_4").html(data);
							$("#upload_status_4").css("color", "#d9534f");
						}
						$(".AjaxDiv4").fadeIn(400);
						setTimeout('$(".AjaxDiv4").fadeOut(400)', 5000);
					}
				});
			}
		});

		$('#portfolio_form_2').on('submit', function(e){
			e.preventDefault();
			if($('#portfolio2').val() == '')
			{
				$('#upload_status_5').html("Please select a file!");
				$('#upload_status_5').css("color", "#999999");
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>profile/portfolio2",
					method:"post",
					data:new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success:function(data){

						if(data == "Success"){
							$("img#msg5").attr("src", "<?php echo base_url();?>assets/Images/success.svg");
							$("#upload_status_5").html("Successfully uploaded!");
							$("#upload_status_5").css("color", "#5cb85c");
						}
						else if(data == "Failed") {
							$("img#msg5").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_5").html("Failed to upload!");
							$("#upload_status_5").css("color", "#d9534f");
						}
						else {
							$("img#msg5").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_5").html(data);
							$("#upload_status_5").css("color", "#d9534f");
						}
						$(".AjaxDiv5").fadeIn(400);
						setTimeout('$(".AjaxDiv5").fadeOut(400)', 5000);
					}
				});
			}
		});

		$('#portfolio_form_3').on('submit', function(e){
			e.preventDefault();
			if($('#portfolio3').val() == '')
			{
				$('#upload_status_6').html("Please select a file!");
				$('#upload_status_6').css("color", "#999999");
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>profile/portfolio3",
					method:"post",
					data:new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success:function(data){

						if(data == "Success"){
							$("img#msg6").attr("src", "<?php echo base_url();?>assets/Images/success.svg");
							$("#upload_status_6").html("Successfully uploaded!");
							$("#upload_status_6").css("color", "#5cb85c");
						}
						else if(data == "Failed") {
							$("img#msg6").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_6").html("Failed to upload!");
							$("#upload_status_6").css("color", "#d9534f");
						}
						else {
							$("img#msg6").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_6").html(data);
							$("#upload_status_6").css("color", "#d9534f");
						}
						$(".AjaxDiv6").fadeIn(400);
						setTimeout('$(".AjaxDiv6").fadeOut(400)', 5000);
					}
				});
			}
		});

		$('#portfolio_form_4').on('submit', function(e){
			e.preventDefault();
			if($('#portfolio4').val() == '')
			{
				$('#upload_status_7').html("Please select a file!");
				$('#upload_status_7').css("color", "#999999");
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>profile/portfolio4",
					method:"post",
					data:new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success:function(data){

						if(data == "Success"){
							$("img#msg7").attr("src", "<?php echo base_url();?>assets/Images/success.svg");
							$("#upload_status_7").html("Successfully uploaded!");
							$("#upload_status_7").css("color", "#5cb85c");
						}
						else if(data == "Failed") {
							$("img#msg7").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_7").html("Failed to upload!");
							$("#upload_status_7").css("color", "#d9534f");
						}
						else {
							$("img#msg7").attr("src", "<?php echo base_url();?>assets/Images/error.svg");
							$("#upload_status_7").html(data);
							$("#upload_status_7").css("color", "#d9534f");
						}
						$(".AjaxDiv7").fadeIn(400);
						setTimeout('$(".AjaxDiv7").fadeOut(400)', 5000);
					}
				});
			}
		});
        
//      Enables the certification button
        $(".fileColumn #certification1").change(function(e){
            var filename = e.target.files[0].name;
            $(".fileColumn textarea[name=certification1]").html(filename);
            
            $("#submitButtonForm1").removeClass("disabled");
            $("#submitButtonForm1 input[type=submit]").removeAttr("disabled");
        });
        $(".fileColumn #certification2").change(function(e){
            var filename = e.target.files[0].name;
            $(".fileColumn textarea[name=certification2]").html(filename);
            
            $("#submitButtonForm2").removeClass("disabled");
            $("#submitButtonForm2 input[type=submit]").removeAttr("disabled");
        });
        $(".fileColumn #certification3").change(function(e){
            var filename = e.target.files[0].name;
            $(".fileColumn textarea[name=certification3]").html(filename);
            
            $("#submitButtonForm3").removeClass("disabled");
            $("#submitButtonForm3 input[type=submit]").removeAttr("disabled");
        });
        
//      Enables the portfolio button
        $(".tab #portfolio1").change(function(e){
            var filename = e.target.files[0].name;
            $(".tab #fileNamePort1").html(filename);
            
            $("#submitButtonPort1").removeClass("disabled");
            $("#submitButtonPort1 input[type=submit]").removeAttr("disabled");
            
            setTimeout(function() {
                $(".tab #fileNamePort1").html("");
                $("#submitButtonPort1").addClass("disabled");
                $("#submitButtonPort1 input[type=submit]").attr("disabled", "disabled");
            }, 20000);
        });
        $(".tab #portfolio2").change(function(e){
            var filename = e.target.files[0].name;
            $(".tab #fileNamePort2").html(filename);
            
            $("#submitButtonPort2").removeClass("disabled");
            $("#submitButtonPort2 input[type=submit]").removeAttr("disabled");
            
            setTimeout(function() {
                $(".tab #fileNamePort2").html("");
                $("#submitButtonPort2").addClass("disabled");
                $("#submitButtonPort2 input[type=submit]").attr("disabled", "disabled");
            }, 20000);
        });
        $(".tab #portfolio3").change(function(e){
            var filename = e.target.files[0].name;
            $(".tab #fileNamePort3").html(filename);
            
            $("#submitButtonPort3").removeClass("disabled");
            $("#submitButtonPort3 input[type=submit]").removeAttr("disabled");
            
            setTimeout(function() {
                $(".tab #fileNamePort3").html("");
                $("#submitButtonPort3").addClass("disabled");
                $("#submitButtonPort3 input[type=submit]").attr("disabled", "disabled");
            }, 20000);
        });
        $(".tab #portfolio4").change(function(e){
            var filename = e.target.files[0].name;
            $(".tab #fileNamePort4").html(filename);
            
            $("#submitButtonPort4").removeClass("disabled");
            $("#submitButtonPort4 input[type=submit]").removeAttr("disabled");
            
            setTimeout(function() {
                $(".tab #fileNamePort4").html("");
                $("#submitButtonPort4").addClass("disabled");
                $("#submitButtonPort4 input[type=submit]").attr("disabled", "disabled");
            }, 20000);
        });
	});
    $(document).ready(function() {

        $("#stateReal").hide();
        var selectedCountry = "none";
        <?php if (isset($country)) : ?><?= 'selectedCountry = "'.$country.'";' ?><?php endif; ?>
        var selectedState = "none";
        <?php if (isset($country)) : ?><?= 'selectedState = "'.$state.'";' ?><?php endif; ?>

		populateCountries("country", "state", selectedCountry);
        if( selectedCountry != "none" ) {
            $("#stateDummy").hide();
			populateStates("country", "state", selectedState);
			$("#stateReal").show();
        }
		var countryElement = document.getElementById("country");
		countryElement.onchange = function(){
			$("#stateDummy").hide();
			populateStates("country", "state", selectedState);
			$("#stateReal").show();
		};
    });
	$(document).ready(function() {
        //option A
        $("#userSearchForm").submit(function(e){
            e.preventDefault();
        });

		$('.tabs .tab-links a').on('click', function(e)  {
			var currentAttrValue = jQuery(this).attr('href');

			// Show/Hide Tabs
			$('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

			// Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            if( currentAttrValue == "#tab1" ) {
                $("#bioEdit").fadeIn(400);
                $(".overviewEditContainer .edit").hide();
                $("#bioEdit").addClass("editActive");
                $("#overviewEdit").removeClass("editActive");
                $(".bioEditContainer .edit").hide();
                $(".bioEditContainer #bioEdit").fadeIn(400);
            } else {
                $(".bioEditContainer .edit").hide();
                $("#overviewEdit").fadeIn(400);
                $("#bioEdit").removeClass("editActive");
                $("#overviewEdit").addClass("editActive");
                $(".overviewEditContainer .edit").hide();
                $(".overviewEditContainer #overviewEdit").fadeIn(400);
            }

			e.preventDefault();
		});

        $('input[name="myimage"]').change(function(){
            var fullpath = $(this).val();
            var backslash=fullpath.lastIndexOf("\\");
            var filename = fullpath.substr(backslash+1);
            $("#dpText").html(filename);
            validate();
        });
	});
    
	// ------------- Search for user by pressing enter ----------- //
    $('.input-group input[type=search]').on('keypress', function (e) {
        if(e.which === 13){
            var link = "<?php echo base_url(); ?>" + "u/" + $('.input-group input[type=search]').val();
            window.location.href =link;
        }
    });
    // ------------- Search for user ----------- //
    function searchUser() {
        var link = "<?php echo base_url(); ?>" + "u/" + $('.input-group input[type=search]').val();
        window.location.href =link;
    }
    
    // ------------- Show dp change modal ----------- //
    function showModal() {
        $(".modal").addClass("is-active");
    }
    // ------------- Hide dp change modal ----------- //
    function hideModal() {
        $(".modal").removeClass("is-active");
    }
    
    // ------------- Toggles information cards ----------- //
	function toggleRightDiv(id) {
        $(".is-card-right #" +id).fadeIn(400).siblings().hide();

        $('#tab2').show().siblings().hide();
        $('.overviewEditContainer').addClass('active').siblings().removeClass('active');
        $(".bioEditContainer .edit").hide();
        $(".overviewEditContainer .edit").hide();
        $(".overviewEditContainer #overviewEdit").show();

		$(".sectionController button").removeClass("is-pressed");
		$(".sectionController #" +id).addClass("is-pressed");
	}
    
    // ------------- Shows edit pane ----------- //
    function showEditPane(id) {
        $(".is-card-right #" +id+ "Wrapper").fadeIn(400).siblings().hide();
    }
    // ------------- Shows edit tab ----------- //
    function showEditTab(id) {
        $(".tab-contents #" +id+ "Tab").fadeIn(400).siblings().hide();
        if( id == "bioEdit" ) {
            $(".bioEditContainer .save").fadeIn(400);
            $(".bioEditContainer .cancel").fadeIn(400);
        } else if( id == "overviewEdit" ) {
            $(".overviewEditContainer .save").fadeIn(400);
            $(".overviewEditContainer .cancel").fadeIn(400);
        }
    }
    // ------------- Submits various forms ----------- //
    function submitForm(id) {
        $("#" +id+ "Form").submit();
    }
    // ------------- Hides various form edit panes ----------- //
    function cancelForm(id) {
        if( id == "descCancel" ) {
            $(".is-card-right #desc").fadeIn(400).siblings().hide();
        } else if( id == "accountCancel" ) {
            $(".is-card-right #account").fadeIn(400).siblings().hide();
        } else if( id == "bioCancel" ) {
            $(".tab-contents #tab1").fadeIn(400).siblings().hide();
            $(".bioEditContainer .edit").hide();
            $(".bioEditContainer #bioEdit").fadeIn(400);
        } else if( id == "overviewCancel" ) {
            $(".tab-contents #tab2").fadeIn(400).siblings().hide();
            $(".overviewEditContainer .edit").hide();
            $(".overviewEditContainer #overviewEdit").fadeIn(400);
        }
    }
    
    // ------------- Download pdf on clicking the icon ----------- //
    function downloadPdf(id) {
        if( id == "pdfFile1") {
            window.open('<?php if (isset($certification_1_pdf)) : ?><?= base_url().$certification_1_pdf ?><?php endif; ?>');
        } else if( id == "pdfFile2" ) {
            window.open('<?php if (isset($certification_2_pdf)) : ?><?= base_url().$certification_2_pdf ?><?php endif; ?>');
        } else if( id == "pdfFile3" ) {
            window.open('<?php if (isset($certification_3_pdf)) : ?><?= base_url().$certification_3_pdf ?><?php endif; ?>');
        }
    }
    
    // ------------- Shows large dp image size error ----------- //
    function validate() {
        $("#file_error").html("");
        var file_size = $('#file')[0].files[0].size;
        if(file_size>2097152) {
            $("#file_error").html("File size is greater than 2MB");
            $('input[name="submit_image"]').attr('disabled', 'disabled');
        } else {
            $('input[name="submit_image"]').removeAttr('disabled');
        }
    }

    // ------------- Copies the selected skills to the input field ----------- //
    function skillFunc() {
        if( $("#skills").val().length != 0 )
            $("#skillsReal").val($("#skills").val());
        return true;
    }
    
    // ------------- Shows image not found error on portfolii ----------- //
    function showErrorImage(source) {
        source.src = '<?= base_url(); ?>assets/Images/NoPicAvailable.jpg';
        source.onerror = "";
        return true;
    }

    // ------------- For skill input field purposed ----------- //
    var skills = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/js/categoryList.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills.initialize();
    $('#skills').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 15,
            displayKey: 'name',
            valueKey: 'name',
            source: skills.ttAdapter()
      }
    });
</script>
</body>

</html>
