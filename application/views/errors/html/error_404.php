<html lang="en-US">
<head>

	<title>Warning</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Gorditas">

	<style>
		body {
			height: 100vh;
			width: 100vw;
			background: #F44336;
		}
		.col {
			height: auto;
			position: absolute;
			top: 50%;
			transform: translateY(-55%);
			color: white;
		}
		.errorMessage {
			font-size: 100px;
			font-family: "Gorditas";
			font-weight: bold;
			text-shadow: 0px 5px 20px rgba(0, 0, 0, 0.5);
		}
		.message {
			font-size: 15px;
			font-family: "Open Sans";
			margin-bottom: 30px;
			text-shadow: 0px 5px 20px rgba(0, 0, 0, 0.5);
		}
		.link {
			text-decoration: none;
			color: white;
			font-size: 1.2em;
			font-family: "Open Sans";
			text-transform: capitalize;
		}
	</style>
</head>

<body>
<div class="row">
	<div class="col s10 offset-s1 m8 offset-m2 l6 offset-l3">

		<!--    404 page error    -->
		<div style='text-align: center;'>
			<div class='center errorMessage'>
				404
			</div>
			<div class='center message'>
				The requested page cannot be found!
			</div>
			<a href='<?php echo base_url();?>' class='link btn black'>
				Go to Home
			</a>
		</div>

	</div>
</div>
</body>
</html>
