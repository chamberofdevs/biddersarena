<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bidder's Arena | Register</title>

	<!-- Bulma files -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">

	<!-- Countries selector js file -->
	<script src="<?php echo base_url();?>assets/js/countries.js"></script>

	<style>
        html {
            overflow: scroll;
            overflow-x: hidden;
        }
        ::-webkit-scrollbar {
            width: 0px;
            background: transparent;
        }
		.navbar-brand a {
			font-family: "Josefin Slab", "Open Sans", sans-serif !important;
			font-size: 1.5em;
			font-weight: bold;
			padding-left: 20px;
			padding-right: 20px;
		}
		.page1,
		.page2 {
			background: white;
			box-shadow: 0px 5px 10px rgba(66, 66, 66, 0.8);
			padding: 20px;
		}
		.page1 label,
		.page2 label {
			color: white;
			font-family: "Open Sans", sans-serif;
		}
		.page1 input,
		.page2 textarea,
		.page2 input,
		.page2 select {
			font-family: "Arvo";
			letter-spacing: 1px;
		}
		.page1 .button,
		.page2 .button {
			width: 30%;
			margin-top: 10px;
		}
		.columns:last-child {
			margin-bottom: 0px;
		}
		.hero-foot .footerCol{
			background: rgba(21, 101, 192, 0.8);
		}
		.control .non-resizable {
			resize: none;
			overflow: hidden;
		}
        
        .tncContainer {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 10;
            height: 100%;
            width: 100%;
            background: rgba(0, 0, 0, 0.4);
        }
        .tncContainer .tnc {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 90%;
            max-height: 480px;
            max-width: 720px;
            background: white;
            padding: #000;
			box-shadow: 0px 5px 10px rgba(66, 66, 66, 0.8);
			padding: 20px;
            text-align: center;
        }
        .tncContainer .tnc .delete {
            position: fixed;
            right: 10px;
            top: 10px;
        }
        .tncContainer .tnc .content {
            overflow-y: scroll;
            max-height: 415px;
            width: 100%;
            margin-top: 20px;
        }
        
        .tnc .headingTitle {
            font-size: 1.5em;
            font-weight: 700;
            text-decoration: underline;
        }
        .tnc .subHeading {
            font-size: 1.2em;
            font-weight: 600;
        }
        .tnc .point {
            font-size: 1em;
            text-decoration: underline;
            font-weight: 600;
        }
        .tnc span {
            display: inline-block;
            font-size: 0.9em;
        }
        .tnc #sub {
            color: red;
            font-weight: bold;
        }
        .tnc #sub::before,
        .tnc #sub::after {
            content: " --- ";
        }
        /* For notification */
		.alertMsg {
			position: absolute;
			z-index: 100;
            min-height: 52px;
			height: auto;
            width: auto;
			text-align: center;
			background: #F44336;
			font-size: 16px;
            padding: 10px 20px;
            left: 50%;
            transform: translateX(-50%);
            display: table;
            border-radius: 0px 0px 5px 5px;
            
            -webkit-box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            color: #4a4a4a;

			animation: fadeOutThreeSec 3000ms ease-out forwards 3000ms;
		}
        @media screen and (max-width: 526px) {
            .alertMsg {
                border-radius: 0px;
            }
        }
        .alertMsg span {
			color: #F5F5F5;
            display: table-cell;
            vertical-align: middle;
        }
		@keyframes fadeOutThreeSec {
			0%{
                top: 0px;
			}
			75%{
                top: 0px;
			}
			100%{
                top: -120px;
			}
		}
	</style>
</head>

<body>

<?php if (validation_errors()) : ?>
    <div class="alertMsg">
        <span><?= validation_errors(); ?></span>
    </div>
<?php endif; ?>
	
<?php if (isset($error)) : ?>
    <div class="alertMsg">
        <span><?= $error ?></span>
    </div>
<?php endif; ?>
	
<section class="hero is-info is-fullheight">
	<div class="hero-head">
		<nav class="navbar is-transparent">
			<div class="navbar-brand">

				<!-- Can also use img tag for logo -->
				<a class="navbar-item" href="<?php echo base_url();?>">
					Bidder's Arena
				</a>
				<div class="navbar-burger burger" data-target="navbarDropDown">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>

			<div id="navbarDropDown" class="navbar-menu">
				<div class="navbar-end has-text-centered">
					<a class="navbar-item" href="<?php echo base_url();?>">Home</a>
					<a class="navbar-item" href="<?php echo base_url();?>login">Login</a>
					<a class="navbar-item" href="#">Contact Us</a>
					<a class="navbar-item" href="#">Our Services</a>
				</div>
			</div>
		</nav>
	</div>

	<div class="hero-body">
		<div class="container">

			<div class="columns is-centered">
				<div class="column is-10-phone is-10-tablet is-8-desktop is-6-widescreen">
					<form action="<?php echo base_url(); ?>signup" method="post">

						<div class="page1">
							<div class="field">
								<label class="label has-text-grey-dark">Name</label>
								<div class="control has-icons-left">
									<input id="name" class="input is-info" type="text" placeholder="Enter name" name="name">
									<span class="icon is-small is-left"><i class="fa fa-users"></i></span>
								</div>
							</div>
							<div class="field">
								<label class="label has-text-grey-dark">Username</label>
								<div class="control has-icons-left">
									<input id="username" class="input is-info" type="text" placeholder="Enter username" name="username">
									<span class="icon is-small is-left"><i class="fa fa-user"></i></span>
								</div>
							</div>
							<div class="field">
								<label class="label has-text-grey-dark">Email</label>
								<div class="control has-icons-left">
									<input id="email" class="input is-info" type="email" placeholder="Enter email" name="email">
									<span class="icon is-small is-left"><i class="fa fa-at"></i></span>
								</div>
							</div>
							<div class="field">
								<label class="label has-text-grey-dark">Password</label>
								<div class="control has-icons-left">
									<input id="password" class="input is-info" type="password" placeholder="Enter password" name="password">
									<span class="icon is-small is-left"><i class="fa fa-lock"></i></span>
								</div>
							</div>
							<div class="field">
								<label class="label has-text-grey-dark">Confirm Password</label>
								<div class="control has-icons-left">
									<input id="password2" class="input is-info" type="password" placeholder="Re-enter password" name="password_confirm">
									<span class="icon is-small is-left"><i class="fa fa-lock"></i></span>
								</div>
							</div>

							<div class="field btnF">
								<div class="control">
									<button type="button" class="button is-danger is-pulled-right" onclick="showPage2();">Next</button>
									<div class="is-clearfix"></div>
								</div>
							</div>
						</div>

						<div class="page2">
							<div class="field">
								<label class="label has-text-grey-dark">Gender</label>
								<div class="control has-icons-left">
									<div class="select is-info is-rounded" style="width: 100%;">
										<select name="gender" required style="width: 100%;">
											<option value="-1">Select gender</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
											<option value="Others">Others</option>
										</select>
									</div>
									<div class="icon is-small is-left">
										<i class="fa fa-venus-mars"></i>
									</div>
								</div>
							</div>
							<div class="field">
								<label class="label has-text-grey-dark">Address</label>
								<div class="control">
									<textarea class="textarea is-info non-resizable" placeholder="Enter address" name="add1" rows="2"></textarea>
								</div>
							</div>
							<div class="field">
								<label class="label has-text-grey-dark">Locality</label>
								<div class="control">
									<textarea class="textarea is-info non-resizable" placeholder="Enter locality" name="add2" rows="1"></textarea>
								</div>
							</div>
							<div class="field">
								<label class="label has-text-grey-dark">Select Country</label>
								<div class="control is-info has-icons-left">
									<div class="select is-rounded" style="width: 100%;">
										<select id="country" name="country" required style="width: 100%;"></select>
									</div>
									<div class="icon is-small is-left">
										<i class="fa fa-globe"></i>
									</div>
								</div>
							</div>
							<div class="field" id ="stateDummy">
								<label class="label has-text-grey-dark">Select State</label>
								<div class="control is-info has-icons-left">
									<div class="select is-rounded" style="width: 100%;">
										<select disabled style="width: 100%;">
											<option>Select State</option>
										</select>
									</div>
									<div class="icon is-small is-left">
										<i class="fa fa-map-marker"></i>
									</div>
								</div>
							</div>
							<div class="field" id="stateReal">             
							    <label class="label has-text-grey-dark">Select State</label>
								<div class="control has-icons-left">
									<div class="select is-info is-rounded" style="width: 100%;">
										<select id="state" name="state" required style="width: 100%;"></select>
									</div>
									<div class="icon is-small is-left">
										<i class="fa fa-map-marker"></i>
									</div>
								</div>
							</div>
							<div class="field">
								<label class="label has-text-grey-dark">City</label>
								<div class="control guide has-icons-left">
									<input class="input is-info" type="text" placeholder="Enter city" name="city">
									<span class="icon is-small is-left"><i class="fa fa-location-arrow"></i></span>
								</div>
							</div>
							<label class="checkbox has-text-grey-dark" style="font-family: 'Open Sans', 'sans-serif;">
                                <input type="checkbox" id="t&c" name="acceptTnC" value="yes">
                                I agree to the <a onclick="toggletnc();" class="has-text-info">terms and conditions</a>
                            </label>

							<div class="field btnF">
								<div class="control">
									<button type="submit" id="subBtn" name="submit" class="button is-info is-pulled-right" disabled>Submit</button>
									<div class="is-clearfix"></div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
            
            <div class="tncContainer">
                <div class="tnc">
                    <a onclick="toggletnc();" class="delete is-pulled-right"></a>
                    <div class="content has-text-grey-dark">
                        <p class="headingTitle">NOTICE OF AGREEMENT</p>
                        <span id="sub">Any participation in this service will constitute acceptance of this agreement</span><br><br>

                        <p class="subHeading sh1">Privacy Policy</p>

                        <span>We have created this privacy policy in order to demonstrate our firm commitment to privacy. The following discloses the information gathering and dissemination practices for our Bidder’s Arena website.</span><br><br>
                        
                        <span>By using the Arena’s site and accepting the User Agreement you also agree to this Privacy Policy. If you do not agree to this Privacy Policy, you must not use our site. The terms "We," "Us," "Our," includes Us  i.e. bidder’s arena member and workers</span><br><br>

                        <p class="point">What information do we collect?</p>
                        <span>Bidder’s Arena collects your information when you register on our site and when you visit our web pages.</span><br><br>
                        
                        <span><span class="point">Personal Information:</span> We may collect the following types of personal information in order to provide you with the use and access to the Arena’s site, services and tools, and for any additional purposes set out in this Privacy Policy.</span><br><br>
                        
                        <span>
                        a&#41; Name, and contact information, such as email address, phone number, mobile telephone number, physical address, and (depending on the service used) sometimes financial information, such as bank account numbers.<br>
                        b&#41; Transactional information based on your activities on the sites (such as bidding, buying, selling, item and content you generate or that relates to your account), billing and other information you provide to purchase an item.<br>
                        c&#41; Personal information you provide to us through correspondence, chats, dispute resolution, or shared by you from other social applications, services or websites.<br>
                        d&#41; Additional personal information we ask you to submit to authenticate yourself if we believe you are violating site policies (for example, we may ask you to send us an ID to answer additional questions online to help verify your identity).<br>
                        e&#41; Information from your interaction with our sites, services, content and advertising, including, but not limited to, device ID, device type, location, geo-location information, computer and connection information, statistics on page views, traffic to and from the sites, ad data, IP address and standard web log information.
                        </span><br><br>
                        
                        <span><span class="point">Non-Personal Information:</span> We may collect non-personal information as you use our website. When you use the site , third-  party service providers (e.g. Google Analytics), and partners may receive and record non-personal information from cookies, server logs, and similar technology from your browser or mobile device, including your IP address.</span><br><br>
                        
                        <span>We may combine some Non-Personal Information with the Personal Information we collect. Where we do so, we will treat the combined information as Personal Information if the resulting combination may be used to readily identify or locate you in the same manner as Personal Information alone.</span><br><br>

                        <p class="point">How we store your information?</p>
                        <span>We use hosting provider, therefore your information may be transferred to and stored on servers in various locations both in and outside of India. We maintain control of your information. We protect your information using technical and administrative security measures to reduce the risks of loss, misuse, unauthorized access, disclosure and alteration. Some of the safeguards we use are firewalls and data encryption, physical access controls to our data centers, and information access authorization controls.</span><br><br>

                        <p class="point">How we use your information?</p>
                        <span>
                        1&#41; When you use the bidder’s arena site, we may request certain information. we does not share any of your personally identifiable or transactional information with any person or entity, other than as set out in this policy. No other third party receives your personally identifiable information or other transactional data except for approximate location / geo-location information when you use the Local jobs service and those with whom you have transactions and share that information. The information we collect is used to improve the content of our web site, used to notify consumers about updates to our web site and for communications, such as customer service.<br><br>
                        
                        2&#41; <span class="point">Email Communications:</span>span We may send you a welcome email to verify your account and other transactional emails for operational purposes, such as billing, account management, or system maintenance. You may only stop those emails by terminating your account. We may also send you promotions, product announcements, surveys, newsletters, developer updates, product evaluations, and event information or other marketing or commercial e-mails. You can opt out of receiving these email communications from us at any time by unsubscribing using the unsubscribe link within each email, updating your e-mail preferences on your Arena’s account or emailing us to have your contact information removed from our email list or registration database.<br><br>

                        3&#41; <span class="point">SMS communications:</span>span We may send you SMS messages to your secure phone number for operational purposes, such as notifying you about the status of your project. You can opt out of receiving these SMS messages from Bidder’s arena at any time by updating your notifications preferences on your Arena’s account or by replying 'STOP' to any message we send.<br><br>

                        4&#41; <span class="point">Marketing:</span>span You agree that we may use your personal information to tell you about our services and tools, deliver targeted marketing and promotional offers based on your communication preferences, and customize measure and improve our advertising. You can unsubscribe from emails at any time by clicking the unsubscribe link contained in the email.<br><br>
                        
                        5&#41; We do not rent, sell, or share Personal Information about you with other people or non- affiliated companies for marketing purposes (including          direct marketing purposes) without your permission.
                        </span><br><br>

                        <p class="point">Updating your Information</p>
                        <span>You can update your information through your account profile settings. If you believe that personal information we hold about you is incorrect, incomplete or inaccurate, then you may request us to amend it.</span><br><br>
                        
                        <span>Additionally, You may request access to any personal information we hold about you at any time by contacting us. Where we hold information that you are entitled to access, we will try to provide you with suitable means of accessing it (for example, by mailing or emailing it to you). </span><br><br>
                        
                        <span>There may be instances where we cannot grant you access to the personal information we hold. For example, we may need to refuse access if granting access would interfere with the privacy of others or if it would result in a breach of confidentiality. If that happens, we will give you written reasons for any refusal.</span><br><br>
                    </div>
                </div>
            </div>
		</div>
	</div>

	<div class="hero-foot">
		<div class="footerCol columns is-mobile has-text-centered">
			<div class="column">
				<p>
					<strong>© </strong><a href="<?php echo base_url();?>">Bidder's Arena</a>.
				</p>
			</div>
			<div class="column">
				<p>
				</p>
			</div>
		</div>
	</div>
</section>

<!-- JQuery -->
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

<!-- Script to make hamburger menu working -->
<script>
	document.addEventListener('DOMContentLoaded', function () {
		// Get all "navbar-burger" elements
		var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

		// Check if there are any navbar burgers
		if ($navbarBurgers.length > 0) {

			// Add a click event on each of them
			$navbarBurgers.forEach(function ($el) {
				$el.addEventListener('click', function () {

					// Get the target from the "data-target" attribute
					var target = $el.dataset.target;
					var $target = document.getElementById(target);

					// Toggle the class on both the "navbar-burger" and the "navbar-menu"
					$el.classList.toggle('is-active');
					$target.classList.toggle('is-active');
				});
			});
		}

	});
</script>

<!-- Script for misc purposes -->
<script>
	$(document).ready(function() {
		$(".page2").hide();
		$("#stateReal").hide();

		populateCountries("country", "state");
		var countryElement = document.getElementById("country");
		countryElement.onchange = function(){
			$("#stateDummy").hide();
			populateStates("country", "state");
			$("#stateReal").show();
		};
        
        var checker = document.getElementById('t&c');
        var btn = document.getElementById('subBtn');
        checker.onchange = function() {
            btn.disabled = !this.checked;
        };
	});
	function showPage2() {
		if( ($('.page1 #name').val().length == 0) || ($('.page1 #username').val().length == 0) || ($('.page1 #email').val().length == 0) || ($('.page1 #password').val().length == 0) || ($('.page1 #password2').val().length == 0) ) {
			alert("Please fill all fields!");
		}else {
			$(".page1").hide();
			$(".page2").show();
			var w = $(".page2 .guide").width();
			$(".page2 select").css("width", w+"px");
		}
	}
    function toggletnc() {
        $(".tncContainer").fadeToggle();
    }
</script>
</body>

</html>

