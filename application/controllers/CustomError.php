<?php

class CustomError extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $this->output->set_status_header('404');
        $this->load->view('errors/error');
    }

    public function error_503() {
    	$this->load->view('errors/error_503');
	}
}
