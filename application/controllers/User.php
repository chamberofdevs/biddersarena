<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('form','url','cookie'));
		$this->load->model('user_model');
        $this->load->model('emailmodel');
	}

	public function index() {

	}

	// ----------------- Registration Module ------------------- //

    public function register() {

		if($this->session->has_userdata('user_id') && $this->session->has_userdata('username') && $this->session->has_userdata('logged_in') ) {

			redirect('home');

		} else {

			redirect('home/#register');

		}
    }

	// Part - Registration Module //

	public function signup() {

		// create the data object

		if($this->session->has_userdata('user_id') && $this->session->has_userdata('username') && $this->session->has_userdata('logged_in') ) {

			redirect('home');

		} else {
			// set validation rules

			$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|xss_clean|min_length[4]|is_unique[user.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[user.email]', array('is_unique' => "Email Already taken, Try with a different one!"));
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[6]', array('min_length' => 'Password too weak, Try entering a long password !'));
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|xss_clean|required|matches[password]', array('matches' => 'Both passwords must match!'));
			$this->form_validation->set_rules('user_type', 'User Type', 'trim|xss_clean|required');

			if ($this->form_validation->run() === false) {

				// validation not ok, send validation errors to the view
				$this->session->set_flashdata('registration_error', validation_errors());
				redirect('home/#register');

			} else {

				// set variables from the form //
				$username = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=', '(', ')'), '', $this->input->post('username')));
				$email = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=' , '(', ')'), '', $this->input->post('email')));
				$password = $this->security->xss_clean($this->input->post('password'));
				$userType = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=', '(', ')', '-', '_'), '', $this->input->post('user_type')));

				if ($this->user_model->create_user($username, $email, $password, $userType)) {
					// user creation ok

					$hash = $this->user_model->get_email_hash($email);
					$this->emailmodel->sendVerificationEmail($email, $hash);
					$this->session->set_flashdata('register_success', 'Account Created Successfully! Check your email for further info');

					redirect('home/#login');

				} else {

					// user creation failed, this should never happen
					$this->session->set_flashdata('registration_error', 'There was a problem creating your new account. Please try again.');
					// send error to the view
					redirect('home/#register');

				}
			}
		}

	}

	// ----------------- End - Registration Module ------------------- //


	// ----------------- Login Module ------------------- //

	public function login() {

		if($this->session->has_userdata('user_id') && $this->session->has_userdata('username') && $this->session->has_userdata('logged_in') ) {

			redirect('home');

		} else {
			$this->load->helper('form');
			$this->load->library('form_validation');

			// set validation rules
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

			if ($this->form_validation->run() == false) {
				// validation not ok, send validation errors to the view

				$this->session->set_flashdata('login_error', validation_errors());
				redirect('home/#login');

			} else {

				// set variables from the form
				$email = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=', '(', ')'), '', $this->input->post('email')));
				$not_encrypt_password = $this->security->xss_clean($this->input->post('password'));
				$password = hash('sha512', $not_encrypt_password);

				if ($this->user_model->resolve_user_login($email, $password)) {

					$result = $this->user_model->check_email_verified($email);

					if($result == true) {
						$user_id = $this->user_model->get_user_id_from_email($email);
						$user    = $this->user_model->get_user($user_id);

						// set session user database

						$this->session->set_userdata('user_id', (int)$user->user_id);
						$this->session->set_userdata('username', (string)$user->username);
						$this->session->set_userdata('logged_in', (bool)true);
						$this->session->set_userdata('is_confirmed', (int)$user->active);
						$this->session->set_userdata('photo', $user->photo);
						$this->session->set_userdata('email', (string)$user->email);

						// user login ok

						$this->session->set_flashdata('msg', 'Logged in successfully!');
						redirect('profile');

					} else {
						$hash = $this->user_model->get_email_hash($email);
						$this->emailmodel->sendVerificationEmail($email, $hash);

						$this->session->set_flashdata('login_error', 'You need to verify your email first! A verification email has been sent to you.');
						// send error to the view
						redirect('/#login');
					}

				} else {

					// login failed
					$this->session->set_flashdata('login_error', 'Wrong Username / Password combination!');
					// send error to the view
					redirect('/#login');

				}
			}
		}
	}

	// ----------------- End - Login Module ------------------- //


	// ----------------- Logout Module ------------------- //

	public function logout() {

		if ($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			// remove session database
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('logged_in');
			$this->session->unset_userdata('is_confirmed');
			$this->session->unset_userdata('photo');
			$this->session->unset_userdata('email');

			delete_cookie('auth_token');

			// user logout ok
			$this->session->set_flashdata('msg', 'Logged Out Successfully');
			redirect('home');

		} else {
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			$this->session->set_flashdata('msg', 'You need to login first');
			redirect('home');

		}

	}

	// ----------------- End - Logout Module ------------------- //


	// ----------------- Password Reset Module ------------------- //


	public function forgetPassword() {
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[6]', array('min_length' => 'Password too weak, Try entering a long password !'));
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]' , array('matches' => 'Both passwords must match!'));
		$data['result'] = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=', '(', ')'), '', $this->input->post('email')));

		if ($this->form_validation->run() === false) {

			// validation not ok, send validation errors to the view
			$this->load->view('user/passwordreset/setnewpassword', $data);

		} else {
			// set variables from the form //
			$not_encrypt_password = $this->security->xss_clean($this->input->post('password'));
			$hash_password = hash('sha512', $not_encrypt_password);

			$result = $this->user_model->update_password($hash_password, $data);
			if($result == true) {
				$data['success'] = true;
				$this->load->view('user/passwordreset/resetSuccess', $data);
			} else {
				$data['success'] = false;
				$this->load->view('user/passwordreset/resetSuccess', $data);
			}
		}
	}

	// Part - Password Reset Module //

	public function passwordreset()
	{
		redirect('/#forgot');
    }

	// Part - Password Reset Module //

    public function passresetreq() {

        $email = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=', '(', ')'), '', $this->input->post('email')));
        $result = $this->user_model->password_reset($email);
		if($result == true) {
			$data['forgot'] = true;
			$this->load->view('user/passwordreset/forgotSuccess', $data);
		} elseif($result == false) {
			$data['forgot'] = false;
			$this->load->view('user/passwordreset/forgotSuccess', $data);
			//show_error($this->email->print_debugger());
		}
    }

	// Part - Password Reset Module //

    public function verifyresethash($password_reset_hash) {

		$v_text = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $password_reset_hash));

		$data['result'] = $this->user_model->verify_reset_hash($v_text);
		if($data['result'] == ''){
			show_error('Invalid Password Reset Link');
        } else {
			$this->load->view('user/passwordreset/setnewpassword', $data);
		}
    }

	// Part - Password Reset Module //

    public function invalidateresetlink()
    {
        $this->user_model->invalidate_reset_link();
    }

    // -------------- End - Password Reset Module ------------------ //


    // -------------- Contact Us Module ------------------ //

    public function feedback(){
		
        $name = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=', '(', ')'), '', $this->input->post('name')));
        $email = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=', '(', ')'), '',$this->input->post('email')));
        $message = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '=', '(', ')'), '',$this->input->post('message')));
        //$this->user_model->add_feedback($name,$email,$message);
        $success = $this->emailmodel->send_feedback_email($name,$email, $message);
        if($success) {
			$this->session->set_flashdata('msg', "SUCCESS");
			redirect('/');
        } else {
            show_error($this->email->print_debugger());
        }
    }

	// -------------- End - Contact Us Module ------------------ //


	// -------------- Registration Ajax Module ------------------ //

	function check_username_availability()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|xss_clean|min_length[4]');

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		}
		else
		{
			if($this->user_model->is_username_available($this->security->xss_clean($this->input->post('username'))))
			{
				echo 'Sorry, that username\'s taken. Try another?';
			}
			else
			{
				echo 'Username Available';
			}
		}
	}

	function check_email_availability()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		}
		else
		{
			if($this->user_model->is_email_available($this->security->xss_clean($this->input->post('email'))))
			{
				echo 'Email Already taken, Try with a different one!';
			}
			else
			{
				echo 'Email Available';
			}
		}
	}

	function check_password_validity()
	{
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_regex_check|xss_clean|min_length[6]', array('min_length' => 'Password too weak, Try entering a long password !'));

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		}
		else
		{
			echo 'Password OK!';
		}
	}

	function password_match()
	{
		$password = $this->security->xss_clean($this->input->post('password'));

		$confirm_password = $this->security->xss_clean($this->input->post('confirm_password'));

		if($password != $confirm_password)
		{
			echo "Both passwords must match!";
		}
		else
		{
			echo 'Password Match!';
		}
	}

	public function regex_check ($str)
	{
		if (1 !== preg_match("/^[A-Za-z0-9_.-@!%]+$/", $str))
		{
			$this->form_validation->set_message('regex_check', 'That symbol cannot go in %s field');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	// -------------- End - Registration Ajax Module ------------------ //

}
