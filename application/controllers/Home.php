<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('homemodel');
		$this->load->library('session');
	}

	public function index() {
		$this->load->view('home');
	}

	public function termsConditions() {
		$this->load->view('terms');
	}

	// -------------- User Verification Module ------------------ //

	function verify ($verificationText) {
		$v_text = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $verificationText));

		$noRecords = (int)$this->homemodel->verifyEmailAddress($v_text);
		if ($noRecords > 0){
			$error = "Email Verified Successfully!";
		} else {
			$error = "Sorry Unable to Verify Your Email!";
		}
		$this->session->set_flashdata('msg', $error);
		redirect('home');
	}

	// -------------- End - User Verification Module ------------------ //

}
